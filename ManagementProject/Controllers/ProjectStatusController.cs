﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManagementProject.Models;

namespace ManagementProject.Controllers
{
    public class ProjectStatusController : Controller
    {
        private readonly ManProContext _context;
        public ProjectStatusController(ManProContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View(await _context.ProjectStatus.ToListAsync());
        }

        //[HttpGet]
        public IActionResult Create()
        {
            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectStatusType = new SelectList(_context.ProjectStatusType, "STATUS_TYPE", "ABBREVIATION");
            ViewBag.ListProjectStep = new SelectList(_context.ProjectStep,"STEP_ID", "STEP_ID");
            ViewBag.ListProject = new SelectList(_context.Project, "PROJECT_ID", "PROJECT_ID");

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create ([Bind("PROJECT_ID, STATUS_ID, SOURCEID, STEP_ID, STATUS")] ProjectStatus projectStatus)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    projectStatus.ROW_CREATED_BY = "SYSTEM  ADD";

                    _context.Add(projectStatus);
                    await _context.SaveChangesAsync();

                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "0" },
                        { "2", "I" },
                        { "3", "projectStatus.STATUS_ID" }
                    };

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "1" },
                        { "2", "I" },
                        { "3", ex.Message }
                    };

                    ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
                    ViewBag.ListProjectStatusType = new SelectList(_context.ProjectStatusType, "STATUS_TYPE", "ABBREVIATION");
                    ViewBag.ListProjectStep = new SelectList(_context.ProjectStep, "STEP_ID", "STEP_ID");
                    ViewBag.ListProject = new SelectList(_context.Project, "PROJECT_ID", "PROJECT_ID");

                    return View();
                }
            }
            return View(projectStatus);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var projectStatus = await _context.ProjectStatus.FindAsync(id);
            if (projectStatus == null)
                return NotFound();

            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectStatusType = new SelectList(_context.ProjectStatusType, "STATUS_TYPE", "ABBREVIATION");
            ViewBag.ListProjectStep = new SelectList(_context.ProjectStep, "STEP_ID", "STEP_ID");
            ViewBag.ListProject = new SelectList(_context.Project, "PROJECT_ID", "PROJECT_ID");

            return View(projectStatus);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("PROJECT_ID,SOURCEID,STATUS_ID,STATUS,STEP_ID")] ProjectStatus projectStatus)
        {
            if (id != projectStatus.STATUS_ID)
                return NotFound();

            if (_context.ProjectStatus.Find(id) == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var local = _context.Set<ProjectStatus>()
                        .Local
                        .FirstOrDefault(x => x.STATUS_ID.Equals(id));

                    local.PROJECT_ID = projectStatus.PROJECT_ID;
                    local.SOURCEID = projectStatus.SOURCEID;
                    local.STATUS = projectStatus.STATUS;
                    local.STEP_ID = projectStatus.STEP_ID;

                    local.ROW_CHANGED_BY = "SYSTEM EDIT";

                    _context.Update(local);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectStatusExists(projectStatus.STATUS_ID))
                        return NotFound();
                    else
                        throw;
                }
                return RedirectToAction(nameof(Index));
            }
            return View(projectStatus);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"Your parameter id is null." }
                };
                return View();
            }
            var projectStatus = await _context.ProjectStatus
                .FirstOrDefaultAsync(m => m.STATUS_ID == id);

            if (projectStatus == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"ID key : {id}, not found." }
                };
                return View();
            }

            projectStatus = await _context.ProjectStatus.FindAsync(id);
            _context.ProjectStatus.Remove(projectStatus);
            await _context.SaveChangesAsync();

            TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "D" },
                    { "3", projectStatus.STATUS_ID }
                };

            return RedirectToAction(nameof(Index));
        }

        private bool ProjectStatusExists(string id)
        {
            return _context.ProjectStatus.Any(e => e.STATUS_ID == id);
        }
    }
}