﻿using ManagementProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementProject.Controllers
{
    public class ProjectTypesController : Controller
    {
        private readonly ManProContext _context;

        public ProjectTypesController(ManProContext context)
        {
            _context = context;
        }

        // GET: ProjectTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.ProjectType.ToListAsync());
        }

        // GET: ProjectTypes/Create
        public IActionResult Create()
        {
            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            return View();
        }

        // POST: ProjectTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SOURCEID","PROJECT_TYPE,ABBREVIATION,LONG_NAME,SHORT_NAME,REMARK")] ProjectType projectType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    projectType.ROW_CREATED_BY = "SYSTEM ADD";

                    _context.Add(projectType);
                    await _context.SaveChangesAsync();

                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "0" },
                        { "2", "I" },
                        { "3", projectType.PROJECT_TYPE }
                    };

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "1" },
                        { "2", "I" },
                        { "3", ex.Message }
                    };

                    ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");

                    return View();
                }
            }
            return View(projectType);
        }

        // GET: ProjectTypes/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();
            
            var projectType = await _context.ProjectType.FindAsync(id);

            if (projectType == null)
                return NotFound();

            ViewBag.ListSource = new SelectList(_context.Source.AsQueryable(), "SOURCEID", "ABBREVIATION");
 
            return View(projectType);
        }

        // POST: ProjectTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("PROJECT_TYPE,SOURCEID,ABBREVIATION,LONG_NAME,SHORT_NAME,REMARK")] ProjectType projectType)
        {
            if (id != projectType.PROJECT_TYPE)
                return NotFound();

            if (_context.ProjectType.Find(id) == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var local = _context.Set<ProjectType>()
                        .Local
                        .FirstOrDefault(x => x.PROJECT_TYPE.Equals(id));

                    local.SOURCEID = projectType.SOURCEID;
                    local.ABBREVIATION = projectType.ABBREVIATION;
                    local.LONG_NAME = projectType.LONG_NAME;
                    local.SHORT_NAME = projectType.SHORT_NAME;
                    local.REMARK = projectType.REMARK;

                    local.ROW_CHANGED_BY = "SYSTEM EDIT";

                    _context.Update(local);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectTypeExists(projectType.PROJECT_TYPE))
                        return NotFound();
                    else
                        throw;
                }

                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "U" },
                    { "3", projectType.PROJECT_TYPE }
                };

                return RedirectToAction(nameof(Index));
            }
            return View(projectType);
        }

        // GET: ProjectTypes/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"Your parameter id is null." }
                };
                return View();
            }

            var projtype = await _context.ProjectType
                .FirstOrDefaultAsync(m => m.PROJECT_TYPE == id);

            if (projtype == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"ID key : {id}, not found." }
                };
                return View();
            }

            projtype = await _context.ProjectType.FindAsync(id);
            _context.ProjectType.Remove(projtype);
            await _context.SaveChangesAsync();

            TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "D" },
                    { "3", projtype.PROJECT_TYPE }
                };

            return RedirectToAction(nameof(Index));
        }

        private bool ProjectTypeExists(string id)
        {
            return _context.ProjectType.Any(e => e.PROJECT_TYPE == id);
        }
    }
}
