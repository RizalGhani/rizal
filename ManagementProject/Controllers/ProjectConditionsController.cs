﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManagementProject.Models;

namespace ManagementProject.Controllers
{
    public class ProjectConditionsController : Controller
    {
        private readonly ManProContext _context;

        public ProjectConditionsController(ManProContext context)
        {
            _context = context;
        }

        // GET: ProjectConditions
        public async Task<IActionResult> Index()
        {
            return View(await _context.ProjectCondition.ToListAsync());
        }

        // GET: ProjectConditions/Create
        public IActionResult Create()
        {
            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");

            return View();
        }

        // POST: ProjectConditions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CONDITION_TYPE,ABBREVIATION,LONG_NAME,SHORT_NAME,PROJECT_TYPE,SOURCEID")] ProjectCondition projectCondition)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    projectCondition.ROW_CREATED_BY = "SYSTEM ADD";

                    _context.Add(projectCondition);
                    await _context.SaveChangesAsync();

                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "0" },
                        { "2", "I" },
                        { "3", projectCondition.CONDITION_TYPE }
                    };

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "1" },
                        { "2", "I" },
                        { "3", ex.Message }
                    };

                    ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
                    ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");

                    return View();
                }
            }
            return View(projectCondition);
        }

        // GET: ProjectConditions/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var projectCondition = await _context.ProjectCondition.FindAsync(id);
            if (projectCondition == null)
                return NotFound();

            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");

            return View(projectCondition);
        }

        // POST: ProjectConditions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CONDITION_TYPE,ABBREVIATION,LONG_NAME,SHORT_NAME,PROJECT_TYPE,SOURCEID")] ProjectCondition projectCondition)
        {
            if (id != projectCondition.CONDITION_TYPE)
                return NotFound();

            if (_context.ProjectCondition.Find(id) == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var local = _context.Set<ProjectCondition>()
                       .Local
                       .FirstOrDefault(x => x.CONDITION_TYPE.Equals(id));

                    local.SOURCEID = projectCondition.SOURCEID;
                    local.PROJECT_TYPE = projectCondition.PROJECT_TYPE;
                    local.ABBREVIATION = projectCondition.ABBREVIATION;
                    local.SHORT_NAME = projectCondition.SHORT_NAME;
                    local.LONG_NAME = projectCondition.LONG_NAME;
                    local.REMARK = projectCondition.REMARK;

                    local.ROW_CHANGED_BY = "SYSTEM EDIT";

                    _context.Update(local);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectConditionExists(projectCondition.CONDITION_TYPE))
                        return NotFound();
                    else
                        throw;
                }

                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "U" },
                    { "3", projectCondition.CONDITION_TYPE }
                };

                return RedirectToAction(nameof(Index));
            }
            return View(projectCondition);
        }

        // GET: ProjectConditions/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"Your parameter id is null." }
                };
                return View();
            }

            var projectCondition = await _context.ProjectCondition
                .FirstOrDefaultAsync(m => m.CONDITION_TYPE == id);

            if (projectCondition == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"ID key : {id}, not found." }
                };
                return View();
            }

            projectCondition = await _context.ProjectCondition.FindAsync(id);
            _context.ProjectCondition.Remove(projectCondition);
            await _context.SaveChangesAsync();

            TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "D" },
                    { "3", projectCondition.CONDITION_TYPE }
                };

            return RedirectToAction(nameof(Index));
        }

        private bool ProjectConditionExists(string id)
        {
            return _context.ProjectCondition.Any(e => e.CONDITION_TYPE == id);
        }
    }
}
