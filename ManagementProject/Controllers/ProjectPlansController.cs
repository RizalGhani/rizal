﻿using ManagementProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementProject.Controllers
{
    public class ProjectPlansController : Controller
    {
        private readonly ManProContext _context;

        public ProjectPlansController(ManProContext context)
        {
            _context = context;
        }

        // GET: ProjectPlans
        public async Task<IActionResult> Index()
        {
            return View(await _context.ProjectPlan.ToListAsync());
        }

        // GET: ProjectPlans/Create
        public IActionResult Create()
        {
            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");
            return View();
        }

        // POST: ProjectPlans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PROJECT_PLAN_ID,DESCRIPTION,PLAN_NAME,PROJECT_TYPE,REMARK,SOURCEID")] ProjectPlan projectPlan)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    projectPlan.ROW_CREATED_BY = "SYSTEM ADD";

                    _context.Add(projectPlan);
                    await _context.SaveChangesAsync();

                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "0" },
                        { "2", "I" },
                        { "3", projectPlan.PROJECT_PLAN_ID }
                    };

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "1" },
                        { "2", "I" },
                        { "3", ex.Message }
                    };

                    ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
                    ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");

                    return View();
                }
            }
            return View(projectPlan);
        }

        // GET: ProjectPlans/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var projectPlan = await _context.ProjectPlan.FindAsync(id);
            if (projectPlan == null)
                return NotFound();

            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");

            return View(projectPlan);
        }

        // POST: ProjectPlans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("PROJECT_PLAN_ID,DESCRIPTION,PLAN_NAME,PROJECT_TYPE,REMARK,SOURCEID")] ProjectPlan projectPlan)
        {
            if (id != projectPlan.PROJECT_PLAN_ID)
                return NotFound();

            if (_context.ProjectPlan.Find(id) == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var local = _context.Set<ProjectPlan>()
                        .Local
                        .FirstOrDefault(x => x.PROJECT_PLAN_ID.Equals(id));

                    local.SOURCEID = projectPlan.SOURCEID;
                    local.PROJECT_TYPE = projectPlan.PROJECT_TYPE;
                    local.PLAN_NAME = projectPlan.PLAN_NAME;
                    local.DESCRIPTION = projectPlan.DESCRIPTION;
                    local.REMARK = projectPlan.REMARK;

                    local.ROW_CHANGED_BY = "SYSTEM EDIT";

                    _context.Update(local);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectPlanExists(projectPlan.PROJECT_PLAN_ID))
                        return NotFound();
                    else
                        throw;
                }

                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "U" },
                    { "3", projectPlan.PROJECT_PLAN_ID }
                };

                return RedirectToAction(nameof(Index));
            }
            return View(projectPlan);
        }

        // GET: ProjectPlans/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"Your parameter id is null." }
                };
                return View();
            }

            var projplan = await _context.ProjectPlan
                .FirstOrDefaultAsync(m => m.PROJECT_PLAN_ID == id);

            if (projplan == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"ID key : {id}, not found." }
                };
                return View();
            }

            projplan = await _context.ProjectPlan.FindAsync(id);
            _context.ProjectPlan.Remove(projplan);
            await _context.SaveChangesAsync();

            TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "D" },
                    { "3", projplan.PROJECT_PLAN_ID }
                };

            return RedirectToAction(nameof(Index));
        }

        private bool ProjectPlanExists(string id)
        {
            return _context.ProjectPlan.Any(e => e.PROJECT_PLAN_ID == id);
        }
    }
}
