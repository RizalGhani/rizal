﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManagementProject.Models;

namespace ManagementProject.Controllers
{
    public class ProjectStatusTypesController : Controller
    {
        private readonly ManProContext _context;

        public ProjectStatusTypesController(ManProContext context)
        {
            _context = context;
        }

        // GET: ProjectStatusTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.ProjectStatusType.ToListAsync());
        }

        // GET: ProjectStatusTypes/Create
        public IActionResult Create()
        {
            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");
            return View();
        }

        // POST: ProjectStatusTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("STATUS_TYPE,ABBREVIATION,LONG_NAME,SHORT_NAME,PROJECT_TYPE,SOURCEID")] ProjectStatusType projectStatusType)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    projectStatusType.ROW_CREATED_BY = "SYSTEM ADD";

                    _context.Add(projectStatusType);
                    await _context.SaveChangesAsync();

                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "0" },
                        { "2", "I" },
                        { "3", projectStatusType.PROJECT_TYPE }
                    };

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "1" },
                        { "2", "I" },
                        { "3", ex.Message }
                    };

                    ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
                    ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");

                    return View();
                }
            }
            return View(projectStatusType);
        }

        // GET: ProjectStatusTypes/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var projectStatusType = await _context.ProjectStatusType.FindAsync(id);
            if (projectStatusType == null)
                return NotFound();

            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectType = new SelectList(_context.ProjectType, "PROJECT_TYPE", "ABBREVIATION");

            return View(projectStatusType);
        }

        // POST: ProjectStatusTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("STATUS_TYPE,ABBREVIATION,LONG_NAME,SHORT_NAME,PROJECT_TYPE,SOURCEID")] ProjectStatusType projectStatusType)
        {
            if (id != projectStatusType.STATUS_TYPE)
                return NotFound();

            if (_context.ProjectStatusType.Find(id) == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var local = _context.Set<ProjectStatusType>()
                       .Local
                       .FirstOrDefault(x => x.STATUS_TYPE.Equals(id));

                    local.SOURCEID = projectStatusType.SOURCEID;
                    local.PROJECT_TYPE = projectStatusType.PROJECT_TYPE;
                    local.ABBREVIATION = projectStatusType.ABBREVIATION;
                    local.SHORT_NAME = projectStatusType.SHORT_NAME;
                    local.LONG_NAME = projectStatusType.LONG_NAME;
                    local.REMARK = projectStatusType.REMARK;

                    local.ROW_CHANGED_BY = "SYSTEM EDIT";

                    _context.Update(local);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectStatusTypeExists(projectStatusType.STATUS_TYPE))
                        return NotFound();
                    else
                        throw;
                }

                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "U" },
                    { "3", projectStatusType.STATUS_TYPE }
                };

                return RedirectToAction(nameof(Index));
            }
            return View(projectStatusType);
        }

        // GET: ProjectStatusTypes/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"Your parameter id is null." }
                };
                return View();
            }

            var projectStatusType = await _context.ProjectStatusType
                .FirstOrDefaultAsync(m => m.STATUS_TYPE == id);

            if (projectStatusType == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"ID key : {id}, not found." }
                };
                return View();
            }

            projectStatusType = await _context.ProjectStatusType.FindAsync(id);
            _context.ProjectStatusType.Remove(projectStatusType);
            await _context.SaveChangesAsync();

            TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "D" },
                    { "3", projectStatusType.STATUS_TYPE }
                };

            return RedirectToAction(nameof(Index));
        }

        private bool ProjectStatusTypeExists(string id)
        {
            return _context.ProjectStatusType.Any(e => e.STATUS_TYPE == id);
        }
    }
}
