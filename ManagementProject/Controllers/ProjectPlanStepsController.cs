﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManagementProject.Models;

namespace ManagementProject.Controllers
{
    public class ProjectPlanStepsController : Controller
    {
        private readonly ManProContext _context;

        public ProjectPlanStepsController(ManProContext context)
        {
            _context = context;
        }

        // GET: ProjectPlanSteps
        public async Task<IActionResult> Index()
        {
            return View(await _context.ProjectPlanStep.ToListAsync());
        }

        //// GET: ProjectPlan/Details/5
        //public JsonResult GetListProjPlan(string id)
        //{
        //    if (id == null)
        //        return Json("NotFound");

        //    var projectPlan = _context.ProjectPlan
        //        .Where(x => x.PROJECT_PLAN_ID == id)
        //        .Select( new { Prodid = x.p });
        //        //.FirstOrDefaultAsync(m => m.PROJECT_PLAN_ID == id);
        //    if (projectPlan== null)
        //        return Json("NotFound");

        //    return Json(projectPlan);
        //}

        // GET: ProjectPlanSteps/Create
        public IActionResult Create()
        {
            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectPlan = new SelectList(_context.ProjectPlan, "PROJECT_PLAN_ID", "PLAN_NAME");
            return View();
        }

        // POST: ProjectPlanSteps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PLAN_STEP_ID,PROJECT_PLAN_ID,DESCRIPTION,STEP_NAME,STEP_RULE,STEP_SEQ_NO,SOURCEID,ROW_CREATED_DATE")] ProjectPlanStep projectPlanStep)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    projectPlanStep.ROW_CREATED_BY = "SYSTEM ADD";

                    _context.Add(projectPlanStep);
                    await _context.SaveChangesAsync();

                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "0" },
                        { "2", "I" },
                        { "3", projectPlanStep.PROJECT_PLAN_ID }
                    };

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "1" },
                        { "2", "I" },
                        { "3", ex.Message }
                    };

                    ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
                    ViewBag.ListProjectPlan = new SelectList(_context.ProjectPlan, "PROJECT_PLAN_ID", "PLAN_NAME");

                    return View();
                }
            }
            return View(projectPlanStep);
        }

        // GET: ProjectPlanSteps/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var projectPlanStep = await _context.ProjectPlanStep.FindAsync(id);
            if (projectPlanStep == null)
                return NotFound();

            ViewBag.ListSource = new SelectList(_context.Source, "SOURCEID", "ABBREVIATION");
            ViewBag.ListProjectPlan = new SelectList(_context.ProjectPlan, "PROJECT_PLAN_ID", "PLAN_NAME");

            return View(projectPlanStep);
        }

        // POST: ProjectPlanSteps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("PLAN_STEP_ID,PROJECT_PLAN_ID,DESCRIPTION,STEP_NAME,STEP_RULE,STEP_SEQ_NO,SOURCEID")] ProjectPlanStep projectPlanStep)
        {
            if (id != projectPlanStep.PLAN_STEP_ID)
                return NotFound();

            if (_context.ProjectPlanStep.Find(id) == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var local = _context.Set<ProjectPlanStep>()
                       .Local
                       .FirstOrDefault(x => x.PLAN_STEP_ID.Equals(id));

                    local.SOURCEID = projectPlanStep.SOURCEID;
                    local.PROJECT_PLAN_ID = projectPlanStep.PROJECT_PLAN_ID;
                    local.STEP_NAME = projectPlanStep.STEP_NAME;
                    local.STEP_RULE = projectPlanStep.STEP_RULE;
                    local.STEP_SEQ_NO = projectPlanStep.STEP_SEQ_NO;
                    local.DESCRIPTION = projectPlanStep.DESCRIPTION;

                    local.ROW_CHANGED_BY = "SYSTEM EDIT";

                    _context.Update(local);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProjectPlanStepExists(projectPlanStep.PLAN_STEP_ID))
                        return NotFound();
                    else
                        throw;
                }

                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "U" },
                    { "3", projectPlanStep.PLAN_STEP_ID }
                };

                return RedirectToAction(nameof(Index));
            }
            return View(projectPlanStep);
        }

        // GET: ProjectPlanSteps/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"Your parameter id is null." }
                };
                return View();
            }

            var projectPlanStep = await _context.ProjectPlanStep
                .FirstOrDefaultAsync(m => m.PLAN_STEP_ID == id);

            if (projectPlanStep == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"ID key : {id}, not found." }
                };
                return View();
            }

            projectPlanStep = await _context.ProjectPlanStep.FindAsync(id);
            _context.ProjectPlanStep.Remove(projectPlanStep);
            await _context.SaveChangesAsync();

            TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "D" },
                    { "3", projectPlanStep.PLAN_STEP_ID }
                };

            return RedirectToAction(nameof(Index));
        }

        private bool ProjectPlanStepExists(string id)
        {
            return _context.ProjectPlanStep.Any(e => e.PLAN_STEP_ID == id);
        }
    }
}
