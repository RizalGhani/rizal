﻿using ManagementProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementProject.Controllers
{
    public class SourcesController : Controller
    {
        private readonly ManProContext _context;

        public SourcesController(ManProContext context)
        {
            _context = context;
        }

        // GET: Sources
        public async Task<IActionResult> Index()
        {
            return View(await _context.Source.ToListAsync());
        }

        // GET: Sources/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Sources/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SOURCEID,ABBREVIATION,LONG_NAME,SHORT_NAME,REMARK")] Source source)
        {
            //fungsi buat nge-check error
            //var errors = ModelState.SelectMany(x => x.Value.Errors.Select(z => z.Exception));  
            if (ModelState.IsValid)
            {
                try
                {
                    source.ROW_CREATED_BY = "SYSTEM ADD";
                    source.ROW_CREATED_DATE = Convert.ToDateTime(DateTime.Now.ToShortDateString());

                    _context.Add(source);
                    await _context.SaveChangesAsync();

                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "0" },
                        { "2", "I" },
                        { "3", source.SOURCEID }
                    };

                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    TempData["resultTrans"] = new Dictionary<string, string>()
                    {
                        { "1", "1" },
                        { "2", "I" },
                        { "3", ex.Message }
                    };

                    return View();
                }
            }
            return View(source);
        }

        // GET: Sources/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var source = await _context.Source.FindAsync(id);

            if (source == null)
                return NotFound();

            return View(source);
        }

        // POST: Sources/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("SOURCEID,ABBREVIATION,LONG_NAME,SHORT_NAME,REMARK")] Source source)
        {
            if (id != source.SOURCEID)
                return NotFound();

            if (_context.Source.Find(id) == null)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    var local = _context.Set<Source>()
                        .Local
                        .FirstOrDefault(entry => entry.SOURCEID.Equals(id));
                  
                    local.SOURCEID = source.SOURCEID;
                    local.ABBREVIATION = source.ABBREVIATION;
                    local.LONG_NAME = source.LONG_NAME;
                    local.SHORT_NAME = source.SHORT_NAME;
                    local.REMARK = source.REMARK;

                    local.ROW_CHANGED_BY = "SYSTEM EDIT";
                    local.ROW_CHANGED_DATE = Convert.ToDateTime(DateTime.Now.ToShortDateString());

                    _context.Entry(local).Property(x => x.PPDM_GUID).IsModified = false;
                    _context.Entry(local).Property(x => x.ROW_CREATED_BY).IsModified = false;
                    _context.Entry(local).Property(x => x.ROW_CREATED_DATE).IsModified = false;

                    _context.Update(local);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SourceExists(source.SOURCEID))
                        return NotFound();
                    else
                        throw;
                }

                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "U" },
                    { "3", source.SOURCEID }
                };

                return RedirectToAction(nameof(Index));
            }

            return View(source);
        }

        // POST: Sources/Delete/5
        //[HttpGet, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"Your parameter id is null." }
                };
                return View(); 
            }
                
            var source = await _context.Source
                .FirstOrDefaultAsync(m => m.SOURCEID == id);

            if (source == null)
            {
                TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "1" },
                    { "2", "D" },
                    { "3", $"ID key : {id}, not found." }
                };
                return View();
            }

            source = await _context.Source.FindAsync(id);
            _context.Source.Remove(source);
            await _context.SaveChangesAsync();

            TempData["resultTrans"] = new Dictionary<string, string>()
                {
                    { "1", "0" },
                    { "2", "D" },
                    { "3", source.SOURCEID }
                };

            return RedirectToAction(nameof(Index)); 
        }

        private bool SourceExists(string id)
        {
            return _context.Source.Any(e => e.SOURCEID == id);
        }
    }
}
