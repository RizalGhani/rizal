﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace ManagementProject.Routing
{
    public class Routes
    {
        public static void Route(IRouteBuilder routeBuilder)
        {
            //routeBuilder.Routes. IgnoreRoute("{resource}.axd/{*pathInfo}");
            routeBuilder.MapRoute(
                name: "default",
                //template: "{controller=sources}/{action=Index}/{id?}"
                //template: "{controller=Dashboard}/{action=Index}/{id?}"
                template: "{controller=ProjectStatus}/{action=Index}/{id?}"
                //template: "{controller=projecttypes}/{action=Index}/{id?}"
                //template: "{controller=ProjectPlans}/{action=Index}/{id?}"
                //template: "{controller=ProjectPlanSteps}/{action=Index}/{id?}"
                );
        }
    }
}
