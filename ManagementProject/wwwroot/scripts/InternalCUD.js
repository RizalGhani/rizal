﻿$(document).ready(function () {
    var titlenotif = "";
    var textnotif = "";
    var addclassnotif = "";
    var typenotif = "";

    if ($("#statusTrans").val() === "0") {

        switch ($("#typeTrans").val()) {

            case "I":
                textnotif = 'Your the data <b>' + $("#infoTrans").val() + '</b> was successfully inserted.';
                break;

            case "U":
                textnotif = 'Your the data <b>' + $("#infoTrans").val() + '</b> was successfully updated.';
                break;

            case "D":
                textnotif = 'Your the data <b>' + $("#infoTrans").val() + '</b> was successfully deleted.';
                break;
        }
 
        new PNotify({
            title: 'Success notice',
            text: textnotif,
            width: "24rem",
            addclass: 'alert alert-success alert-styled-left',
            type: 'success',
            delay: 4000
        });
    }
    else if($("#statusTrans").val() === "1"){

        switch ($("#typeTrans").val()) {

            case "I":
                textnotif = "Something problem with inserted process the data. <br \>" +
                    "The data by key : " + $("#infoTrans").val() + " was failed."; 
                break;

            case "U":
                textnotif = "Something problem with updated process the data. <br \>" +
                    "The data by key : " + $("#infoTrans").val() + " was failed."; 
                break;

            case "D":
                textnotif = "Something problem with deleted process the data. <br \>" +
                    "The data by key : " + $("#infoTrans").val() + " was failed."; 
                break;
        }

        new PNotify({
            title: 'Error notice',
            text: textnotif,
            width: "30rem",
            addclass: 'alert alert-warning alert-styled-left',
            type: 'error',
            delay: 30000
        });
    }
});

function getTitleRow(rows) {
    $("#lblpkfield").empty();
    getobject = $(rows).parent().find('input[type=hidden]');
    $("#lblpkfield").text($($(getobject)[0]).attr('name'));
    addEvDelButton($($(getobject)[1]).attr('name'));
}

function addEvDelButton(param) {
    $("#delconfirm").click(function () {
        location.href = param;
        $("#mycancel").click();
    });
}