﻿

var JqueryUiComponents = function () {

    // Autiocomplete
    var _componentUiAutocomplete = function () {
        if (!$().autocomplete) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        // Add demo data
        var availableTags = [
            'ActionScript',
            'AppleScript',
            'Asp',
            'BASIC',
            'C',
            'C++',
            'Clojure',
            'COBOL',
            'ColdFusion',
            'Erlang',
            'Fortran',
            'Groovy',
            'Haskell',
            'Java',
            'JavaScript',
            'Lisp',
            'Perl',
            'PHP',
            'Python',
            'Ruby',
            'Scala',
            'Scheme'
        ];

        // Basic example
        $('#ac-basic').autocomplete({
            source: availableTags
        });


        // Remote data
        //

        //// Remote data
        //$('#ac-remote').autocomplete({
        //    minLength: 2,
        //    source: function (request, response) {
        //        console.log(request);
        //        $.ajax({
        //            url: "ProjectPlans/Test?param=" + request,
        //            contentType: "application/json; charset=utf-8",
        //            datatype: "json",
        //            async: false,
        //            success: function (data) {
        //                console.log(data);
        //                //alert(data);
        //                $.each(data, function (i, item) {
        //                    console.log("sas");
        //                });
        //                //items = [];
        //                //map = {};
        //                //$.each(data.d, function (i, item) {
        //                //    var id = item.split('_')[1];
        //                //    var name = item.split('_')[0];
        //                //    map[name] = { id: id, name: name };
        //                //    items.push(name);
        //                //});
        //                //response(items);
        //                //$(".dropdown-menu").css("height", "auto");
        //            },
        //            error: function (response) {
        //                alert(response.responseText);
        //            },
        //            failure: function (response) {
        //                alert(response.responseText);
        //            }
        //        });
        //    },
        //    search: function () {
        //        $(this).parent().addClass('ui-autocomplete-processing');
        //    },
        //    open: function () {
        //        $(this).parent().removeClass('ui-autocomplete-processing');
        //    }
        //});
        //$('#ac-remote').autocomplete({
        //    minLength: 2,
        //    source: '../../../../global_assets/demo_data/jquery_ui/autocomplete.php',
        //    search: function () {
        //        $(this).parent().addClass('ui-autocomplete-processing');
        //    },
        //    open: function () {
        //        $(this).parent().removeClass('ui-autocomplete-processing');
        //    }
        //});

        //// Remote data with caching
        //var cache = {};
        //$('#ac-caching').autocomplete({
        //    minLength: 2,
        //    source: function (request, response) {
        //        var term = request.term;
        //        if (term in cache) {
        //            response(cache[term]);
        //            return;
        //        }

        //        $.getJSON('../../global_assets/demo_data/jquery_ui/autocomplete.php', request, function (data, status, xhr) {
        //            cache[term] = data;
        //            response(data);
        //        });
        //    },
        //    search: function () {
        //        $(this).parent().addClass('ui-autocomplete-processing');
        //    },
        //    open: function () {
        //        $(this).parent().removeClass('ui-autocomplete-processing');
        //    }
        //});
    };

    var _componentUiDialog = function () {
        if (!$().dialog) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        $('#source-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#abbr-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#short-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#long-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#remark-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //--------
        $('#projtype-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //--------
        $('#projplan-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#projplanname-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });
        
        $('#projdesc-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //--------
        $('#planstepID-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#planstepName-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#planstepRule-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#planstepSeq-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //--------
        $('#stattype-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //--------
        $('#projstatus-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //--------
        $('#condtype-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //
        // Dialog openers
        //
        $('#source-inf-opener').on('click', function () {
            $('#source-inf').dialog('open');
        });

        $('#abbr-inf-opener').on('click', function () {
            $('#abbr-inf').dialog('open');
        });

        $('#short-inf-opener').on('click', function () {
            $('#short-inf').dialog('open');
        });

        $('#long-inf-opener').on('click', function () {
            $('#long-inf').dialog('open');
        });

        $('#remark-inf-opener').on('click', function () {
            $('#remark-inf').dialog('open');
        });

        //------
        $('#projtype-inf-opener').on('click', function () {
            $('#projtype-inf').dialog('open');
        });

        //-----
        $('#projplan-inf-opener').on('click', function () {
            $('#projplan-inf').dialog('open');
        });

        $('#projplanname-inf-opener').on('click', function () {
            $('#projplanname-inf').dialog('open');
        });

        $('#projdesc-inf-opener').on('click', function () {
            $('#projdesc-inf').dialog('open');
        });

        //------
        $('#planstepID-inf-opener').on('click', function () {
            $('#planstepID-inf').dialog('open');
        });

        $('#planstepName-inf-opener').on('click', function () {
            $('#planstepName-inf').dialog('open');
        });

        $('#planstepRule-inf-opener').on('click', function () {
            $('#planstepRule-inf').dialog('open');
        });

        $('#planstepSeq-inf-opener').on('click', function () {
            $('#planstepSeq-inf').dialog('open');
        });
        
        //------
        $('#stattype-inf-opener').on('click', function () {
            $('#stattype-inf').dialog('open');
        });

        //------
        $('#projstatus-inf-opener').on('click', function () {
            $('#projstatus-inf').dialog('open');
        });
        
        //------
        $('#condtype-inf-opener').on('click', function () {
            $('#condtype-inf').dialog('open');
        });
        
        

        //$('#desc-inf-opener').on('click', function () {
        //    $('#desc-inf').dialog('open');
        //});



 
    };

    // PNotify
    var _componentPnotify = function () {
        if (typeof PNotify === 'undefined') {
            console.warn('Warning - pnotify.min.js is not loaded.');
            return;
        }
    };

    var _confirmDeleteMessage = function () {
        $("#savebutton").click(function () {
            $.ajax({
                type: "POST",
                url: $($(getobject)[1]).attr('name'), 
                contentType: "application/json; charset=utf-8",
                async: true,
                dataType: "json",
                success: function (data) {
                    if (data === "") {
                        new PNotify({
                            title: 'Success notice',
                            text: 'Your the data <b>' + $("#lblpkfield").text() + '</b> was successfully deleted.',
                            addclass: 'alert alert-success alert-styled-left',
                            type: 'success',
                            delay: 2000,
                            before_close: function (notice) {
                                window.location.href = $($(getobject)[2]).attr('name');
                            }
                        });
                        $("#mycancel").click();
                        
                    }
                    else {
                        new PNotify({
                            title: 'Warning notice',
                            text: 'Something wrong with process...',
                            addclass: 'alert alert-warning alert-styled-right',
                            type: 'error'
                        });
                    }
                    console.log(data);
                },
                error: function (response) {
                    alert(response.responseText);
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        });
    };

    return {
        init: function () {
            //_componentUiAutocomplete();
            //_componentPnotify();
            _componentUiDialog();
            //_confirmDeleteMessage();
            
        }
    };

}();

// Initialize module
// ------------------------------
document.addEventListener('DOMContentLoaded', function () {
    JqueryUiComponents.init();
});

