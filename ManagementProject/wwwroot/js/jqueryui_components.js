/* ------------------------------------------------------------------------------
 *
 *  # jQuery UI Widgets
 *
 *  Demo JS code for jqueryui_components.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var JqueryUiComponents = function() {

    //
    // Setup module components
    //

    // Dialog
    var _componentUiDialog = function() {
        if (!$().dialog) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        $('#source-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#abbr-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#short-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        $('#long-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });
        
        $('#remark-inf').dialog({
            autoOpen: false,
            modal: true,
            width: '50%',
            resizable: false,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 500
            }
        });

        //
        // Dialog openers
        //
        
        $('#source-inf-opener').on('click', function() {
            $('#source-inf').dialog('open');
        });

        $('#abbr-inf-opener').on('click', function () {
            $('#abbr-inf').dialog('open');
        });

        $('#short-inf-opener').on('click', function () {
            $('#short-inf').dialog('open');
        });

        $('#long-inf-opener').on('click', function () {
            $('#long-inf').dialog('open');
        });

        $('#remark-inf-opener').on('click', function () {
            $('#remark-inf').dialog('open');
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentUiDialog();
        }
    };
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    JqueryUiComponents.init();
});
