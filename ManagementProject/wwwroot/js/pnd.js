﻿$(document).ready(function(){
    $(".create-new-tab-js").click(function(){
        $("ul#tab-new-link").append("<li class='nav-item'>" +
                "<a href='#basic-justified-tab2' class='nav-link nav-link-pnd ' data-toggle='tab'>" +										
                "<span class='font-weight-normal'>DATA002</span> " +
                "<i id='close-tab' class='icon-cross3 ml-2 text-right position-absolute mt-2 dropdown-menu-right' title='Close the tab'></i>" +
                "</a>" +
            "</li>");
    });

	// var $rows = $('ul#mainMenuSecondary li');
	var $parent = $('ul#mainMenuSecondary li');
	var $child = $('ul#mainMenuSecondary li a');
	var $grandChild = $('ul#mainMenuSecondary li a ul li');
	var $nextgrandChild = $('ul#mainMenuSecondary li ul li');

	$('#secondaryMenuSearch').keyup(function() {
		var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
		
		//source by
		//https://stackoverflow.com/questions/9127498/how-to-perform-a-real-time-search-and-filter-on-a-html-table
		
		// $rows.show().filter(function() {
		//     var text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
		//     return !~text.indexOf(val);
		// }).hide();

		var text = '';
		function searchIn(){
			$parent.show().filter(function() {
				text = $(this).text().replace(/\s+/g, ' ').toLowerCase();
				return !~text.indexOf(val);
			}).hide();
		}

		if($parent != 0){
			searchIn();
			$nextgrandChild.css({
				// "display":" block",
				"color": "red"
			});
		}else if($child != 0){
			searchIn();
		}else if($grandChild != 0 ){
			searchIn();
		}
	});



	function changeTheme(){
		$chgTheme = $("#changeTheme");
		$chgThemeDiv = $(".change-theme");
		$nbrCurrent = $(".navbar-light");
		$chgTheme.click(function(){
			$nbrCurrent.removeClass($nbrCurrent);
			$chgThemeDiv.addClass("navbar-dark");
		});
	}
    changeTheme();

    $(".piep-production").owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".prognosis-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".detail-asset-production-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 2,
                nav: true,
                loop: false
            },
            1024: {
                items: 2,
                nav: true,
                loop: false
            },
            1920: {
                items: 2,
                nav: true,
                loop: false
            }
        }
    });

    $(".asset-asia-production-performance-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".asset-malaysia-lpo-summarie-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".asset-africa-production-performance-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".asset-algeria-lpo-summaries-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".prognosis-production-ytd-based-on-intial-rkap-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".prognosis-production-ytd-based-on-revised-rkap-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 2,
                nav: true,
                loop: false
            },
            1024: {
                items: 2,
                nav: true,
                loop: false
            },
            1920: {
                items: 2,
                nav: true,
                loop: false
            }
        }
    });

    $(".piep-oil-production-profile-ytd-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".malaysia-net-oil-production-profile-current-week-performance-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });

    $(".algeria-net-oil-production-profile-current-week-performance-owl").owlCarousel({
        loop: true,
        margin: 10,
        navText: ['<span class="navigate">Navigate</span> : <i class="fa fa-angle-left navigate" aria-hidden="true"></i>', '<i class="fa fa-angle-right navigate" aria-hidden="true"></i>'],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 1,
                nav: false
            },
            1000: {
                items: 1,
                nav: true,
                loop: false
            },
            1024: {
                items: 1,
                nav: true,
                loop: false
            },
            1920: {
                items: 1,
                nav: true,
                loop: false
            }
        }
    });


    $('.navbar a.dropdown-navi').on('click', function (event) {
        $(this).siblings(".dropdown-menu.dashboard-search").toggleClass('show');
    });

    //$('body').on('click', function (e) {
    //    if (!$('li.dropdown.mega-dropdown').is(e.target)
    //        && $('li.dropdown.mega-dropdown').has(e.target).length === 0
    //        && $('.open').has(e.target).length === 0
    //    ) {
    //        $('li.dropdown.mega-dropdown').removeClass('open');
    //    }
    //});


    /* calling zooming */
    $(".xzoom").xzoom({
        // top, left, right, bottom, inside, lens, fullscreen, #ID
        position: 'right',
        // inside, fullscreen
        mposition: 'inside',
        // In the HTML structure, this option gives an ability to output xzoom element, to the end of the document body or relative to the parent element of main source image.
        rootOutput: true,
        // x/y offset
        Xoffset: 5,
        Yoffset: 150,
        // Fade in effect, when zoom is opening.
        fadeIn: true,
        // Fade transition effect, when switching images by clicking on thumbnails.
        fadeTrans: true,
        // Fade out effect, when zoom is closing.
        fadeOut: false,
        // Enable smooth effects
        smooth: true,
        // Smooth move effect of the big zoomed image in the zoom output window.
        // The higher value will make movement smoother.
        smoothZoomMove: 5,
        // Smooth move effect of lens.
        smoothLensMove: 1,
        // Smooth move effect of scale.
        smoothScale: 6,
        // From -1 to 1, that means -100% till 100% scale
        defaultScale: 0,
        // Scale on mouse scroll.
        scroll: true,
        // Tint color. Color must be provided in format like "#color".
        tint: "#000",
        // Tint opacity from 0 to 1.
        tintOpacity: 0.5,
        // Lens color. Color must be provided in format like "#color".
        lens: false,
        // Lens opacity from 0 to 1.
        lensOpacity: 0.5,
        // 'box', 'circle'
        lensShape: 'box',
        // Custom width of zoom window in pixels.
        zoomWidth: '200',
        // Custom height of zoom window in pixels.
        zoomHeight: '400',
        // Class name for source "div" container.
        sourceClass: 'xzoom-source',
        // Class name for loading "div" container that appear before zoom opens, when image is still loading.
        loadingClass: 'xzoom-loading',
        // Class name for lens "div".
        lensClass: 'xzoom-lens',
        // Class name for zoom window(div).
        zoomClass: 'xzoom-preview',
        // Class name that will be added to active thumbnail image.
        activeClass: 'xactive',
        // With this option you can make a selection action on thumbnail by hover mouse point on it.
        hover: false,
        // Adaptive functionality.
        adaptive: true,
        // When selected position "inside" and this option is set to true, the lens direction of moving will be reversed.
        lensReverse: false,
        // Same as lensReverse, but only available when adaptive is true.
        adaptiveReverse: false,
        // Lens will collide and not go out of main image borders. This option is always false for position "lens".
        lensCollision: true,
        //  Output title/caption of the image, in the zoom output window.
        title: false,
        // Class name for caption "div" container.
        titleClass: 'xzoom-caption',
        // Zoom image output as background
        bg: false
    });

    function zooming() {
        // Function that is called on initialization step, that is binding an event "mouseenter" to the passed jQuery object "element" which is originnaly a main source image.
        //  And giving a function that need to be called when event will be triggered "instance.openzoom".
        instance.eventopen = function (element) {
            element.bind('mouseenter', instance.openzoom);
        }

        // Function that is called on the step when it is needed to bind an event to the element on which the leaving will be tracked.
        instance.eventleave = function (element) {
            element.bind('mouseleave', instance.closezoom);
        }

        // Function that is called on the step when it is needed to bind an event to the element on which the moving will be tracked.
        instance.eventmove = function (element) {
            element.bind('mousemove', instance.movezoom);
        }

        // Function that is called on the step when it is needed to bind an event to the element on which the scrolling for scale will be tracked.
        instance.eventscroll = function (element) {
            element.bind('mousewheel DOMMouse<a href="https://www.jqueryscript.net/tags.php?/Scroll/">Scroll</a>', instance.xscroll);
        }

        // Function that is called on the step when it is needed to bind an event to the element on which the click will be tracked.
        instance.eventclick = function (element) {
            element.bind('click', function (event) {
                $('#main_image').trigger('click');
            });
        }

        // Function to open zoom, uses event.pageX and event.pageY as start location of lens.
        instance.openzoom(event)

        // Has no parameters, and simply closing the zoom.
        instance.closezoom()

        // This function also uses event.pageX and event.pageY to be able to provide movement of lens on the zoom.
        instance.movezoom(event)

        // This function is adapted for "mousewheel", "DOMMouseScroll" events and tested, and works fine on all major browsers. 
        // But if you are using some custom events where you will call this function directly then you can use event.xdelta as delta with value 0 or 1 to tell the direction of scaling by 5% from each call, or event.xscale for exact scale with value from -1 to 1 (which means -100% to 100%) to set up exact scale of the zoom as you need.
        instance.xscroll(event)

        // Function that will delete original instance.event* functions by replacing them with blank functions and unbind default events on objects. 
        // It can help you to clear default mouse binding events that xzoom already setup.
        instance.eventunbind()

        // Function that will restore original instance.event* functions.
        instance.eventdefault()
    }

    function init() {
        new WOW().init();
        $("#tree-menus").fancytree();

        $('.carousel').carousel();

        Waves.attach('.button-waves');
        Waves.init();
        zooming();
    }
    init();
});