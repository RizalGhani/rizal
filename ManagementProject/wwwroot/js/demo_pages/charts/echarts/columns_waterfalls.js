/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Column and Waterfall charts
 *
 *  Demo JS code for echarts_columns_waterfalls.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var EchartsColumnsWaterfalls = function() {


    //
    // Setup module components
    //

    // Column and waterfall charts
    var _columnsWaterfallsExamples = function() {
        if (typeof echarts == 'undefined') {
            console.warn('Warning - echarts.min.js is not loaded.');
            return;
        }

        // Define elements
        var columns_basic_element = document.getElementById('columns_basic');
        var columns_stacked_element = document.getElementById('columns_stacked');
        var piep_gas_production_current_week_performance_element = document.getElementById('piep-gas-production-current-week-performance');
        var piep_oil_production_current_week_performance_element = document.getElementById('piep-oil-production-current-week-performance');
        var piep_total_production_current_week_performance_element = document.getElementById('piep-total-production-current-week-performance');
               
        var columns_thermometer_element = document.getElementById('columns_thermometer');
        var columns_clustered_element = document.getElementById('columns_clustered');
        var columns_compositive_waterfall_element = document.getElementById('columns_compositive_waterfall');

        var columns_change_waterfall_element = document.getElementById('columns_change_waterfall');
        var asset_malaysia_oil_production_profile_element = document.getElementById('asset_malaysia_oil_production_profile');
        var asset_malaysia_gas_production_profile_element = document.getElementById('asset-malaysia-gas-production-profile');


        var aset_algeria_oil_production_profile_element = document.getElementById('aset_algeria_oil_production_profile');
        var aset_algeria_gas_production_profile_element = document.getElementById('aset_algeria_gas_production_profile');

        var asset_iraq_oil_production_profile_element = document.getElementById('asset_iraq_oil_production_profile');

        var piep_oil_production_profile_ytd_element = document.getElementById('piep_oil_production_profile_ytd');
        var piep_gas_production_profile_ytd_element = document.getElementById('piep_gas_production_profile_ytd');
        var piep_oil_equivalent_profile_ytd_element = document.getElementById('piep_oil_equivalent_profile_ytd');

        var columns_timeline_element = document.getElementById('columns_timeline');
        var detail_asset_production_element = document.getElementById('detail-asset-production');

        var basecolorred = "#DC143C";
        var basecolorgreen = "#43A047";
        var basecolororange = "#F4511E";
        var basecoloryellow = "#f1c40f";
        var basecolorblue = "#1E88E5";
        var basecolorpurple = "#8e44ad";
        var basecolorpink = "#D81B60";
        var basecolorviolet = "#8E24AA";
        var basecolorteal = "#00897B";

        //
        // Charts configuration
        //

        // Basic columns chart
        if (columns_basic_element) {

            // Initialize chart
            var columns_basic = echarts.init(columns_basic_element);


            //
            // Chart config
            //

            // Options
            columns_basic.setOption({

                // Define colors
                color: ['#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 40,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Evaporation', 'Precipitation'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Evaporation',
                        type: 'bar',
                        data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    position: 'top',
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    },
                    {
                        name: 'Precipitation',
                        type: 'bar',
                        data: [2.6, 5.9, 9.0, 26.4, 58.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3],
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    position: 'top',
                                    textStyle: {
                                        fontWeight: 500
                                    }
                                }
                            }
                        },
                        markLine: {
                            data: [{type: 'average', name: 'Average'}]
                        }
                    }
                ]
            });
        }

        // Stacked columns
        if (columns_stacked_element) {

            // Initialize chart
            var columns_stacked = echarts.init(columns_stacked_element);


            //
            // Chart config
            //

            // Options
            columns_stacked.setOption({

                // Define colors
                color: ['#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Stacked columns
        if (piep_gas_production_current_week_performance_element) {

            // Initialize chart
            var piep_gas_production_current_week_performance = echarts.init(piep_gas_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_gas_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Stacked columns
        if (piep_oil_production_current_week_performance_element) {

            // Initialize chart
            var piep_oil_production_current_week_performance = echarts.init(piep_oil_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_oil_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Stacked columns
        if (piep_total_production_current_week_performance_element) {

            // Initialize chart
            var piep_total_production_current_week_performance = echarts.init(piep_total_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_total_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Thermometer
        if (columns_thermometer_element) {

            // Initialize chart
            var columns_thermometer = echarts.init(columns_thermometer_element);


            //
            // Chart config
            //

            // Options
            var columns_thermometer_options = {

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Actual', 'Forecast'],
                    itemHeight: 8,
                    itemGap: 20,
                    selectedMode: false
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        return params[0].name + '<br/>'
                        + params[0].seriesName + ': ' + params[0].value + '<br/>'
                        + params[1].seriesName + ': ' + (params[1].value + params[0].value);
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Cosco', 'CMA', 'APL', 'OOCL', 'Wanhai', 'Zim', 'Maersk', 'Hanjin', 'Nyk'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    boundaryGap: [0, 0.1],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Actual',
                        type: 'bar',
                        stack: 'sum',
                        barCategoryGap: '50%',
                        itemStyle: {
                            normal: {
                                color: '#FF7043',
                                barBorderColor: '#FF7043',
                                barBorderWidth: 6,
                                label: {
                                    show: true,
                                    position: 'insideTop'
                                }
                            }
                        },
                        data: [260, 200, 220, 120, 100, 80, 130, 230, 90]
                    },
                    {
                        name: 'Forecast',
                        type: 'bar',
                        stack: 'sum',
                        itemStyle: {
                            normal: {
                                color: '#f5f5f5',
                                barBorderColor: '#FF7043',
                                barBorderWidth: 6,
                                label: {
                                    show: true, 
                                    position: 'top',
                                    formatter: function (params) {
                                        for (var i = 0, l = columns_thermometer_options.xAxis[0].data.length; i < l; i++) {
                                            if (columns_thermometer_options.xAxis[0].data[i] == params.name) {
                                                return columns_thermometer_options.series[0].data[i] + params.value;
                                            }
                                        }
                                    },
                                    textStyle: {
                                        color: '#FF7043'
                                    }
                                }
                            }
                        },
                        data: [40, 80, 50, 80,80, 70, 60, 90, 120]
                    }
                ]
            };

            // Set options
            columns_thermometer.setOption(columns_thermometer_options);
        }

        // Stacked clustered columns
        if (columns_clustered_element) {

            // Initialize chart
            var columns_clustered = echarts.init(columns_clustered_element);


            //
            // Chart config
            //

            // Options
            columns_clustered.setOption({

                // Define colors
                color: ['#2ec7c9','#b6a2de','#5ab1ef','#ffb980','#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 5,
                    top: 55,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: [
                        'Version 1.7 - 2k data','Version 1.7 - 2w data','Version 1.7 - 20w data','',
                        'Version 2.0 - 2k data','Version 2.0 - 2w data','Version 2.0 - 20w data'
                    ],
                    itemHeight: 2,
                    itemGap: 8,
                    textStyle: {
                        padding: [0, 10]
                    }
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    }
                },

                // Horizontal axis
                xAxis: [
                    {
                        type: 'category',
                        data: ['Line','Bar','Scatter','Pies','Map'],
                        axisLabel: {
                            color: '#333'
                        },
                        axisLine: {
                            lineStyle: {
                                color: '#999'
                            }
                        },
                        splitLine: {
                            show: true,
                            lineStyle: {
                                color: '#eee',
                                type: 'dashed'
                            }
                        }
                    },
                    {
                        type: 'category',
                        axisLine: {show:false},
                        axisTick: {show:false},
                        axisLabel: {show:false},
                        splitArea: {show:false},
                        splitLine: {show:false},
                        data: ['Line','Bar','Scatter','Pies','Map']
                    }
                ],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333',
                        formatter: '{value} ms'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Version 2.0 - 2k data',
                        type: 'bar',
                        z: 2,
                        itemStyle: {
                            normal: {
                                color: '#F44336',
                                label: {
                                    show: true,
                                    padding: 5,
                                    position: 'top',
                                    textStyle: {
                                        color: '#fff',
                                        fontSize: 12
                                    }
                                }
                            }
                        },
                        data: [247, 187, 95, 175, 270]
                    },
                    {
                        name: 'Version 2.0 - 2w data',
                        type: 'bar',
                        z: 2,
                        itemStyle: {
                            normal: {
                                color: '#4CAF50',
                                label: {
                                    show: true,
                                    padding: 5,
                                    position: 'top',
                                    textStyle: {
                                        color: '#fff',
                                        fontSize: 12
                                    }
                                }
                            }
                        },
                        data: [488, 415, 405, 340, 328]
                    },
                    {
                        name: 'Version 2.0 - 20w data',
                        type: 'bar',
                        z: 2,
                        itemStyle: {
                            normal: {
                                color: '#2196F3',
                                label: {
                                    show: true,
                                    padding: 5,
                                    position: 'top',
                                    textStyle: {
                                        color:'#fff',
                                        fontSize: 12
                                    }
                                }
                            }
                        },
                        data: [906, 911, 908, 778, 550]
                    },
                    {
                        name: 'Version 1.7 - 2k data',
                        type: 'bar',
                        z: 1,
                        xAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#E57373',
                                label: {
                                    show: true,
                                    padding: 5,
                                    position: 'top'
                                }
                            }
                        },
                        data: [680, 819, 564, 724, 890]
                    },
                    {
                        name: 'Version 1.7 - 2w data',
                        type: 'bar',
                        z: 1,
                        xAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#81C784',
                                label: {
                                    show: true,
                                    padding: 5,
                                    position: 'top'
                                }
                            }
                        },
                        data: [1212, 2035, 1620, 955, 1300]
                    },
                    {
                        name: 'Version 1.7 - 20w data',
                        type: 'bar',
                        z: 1,
                        xAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                color: '#64B5F6',
                                label: {
                                    show: true,
                                    padding: 5,
                                    position: 'top'
                                }
                            }
                        },
                        data: [2200, 3000, 2500, 3000, 2000]
                    }
                ]
            });
        }

        // Multiple areas
        if (columns_compositive_waterfall_element) {

            // Initialize chart
            var columns_compositive_waterfall = echarts.init(columns_compositive_waterfall_element);


            //
            // Chart config
            //

            // Options
            columns_compositive_waterfall.setOption({

                // Define colors
                color: ['#f17a52', '#03A9F4'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar = params[0];
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'Golok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data:[0, 3500, 3000, 2300, 1700, 900, 400, 0]
                        data: [0, 17718, 16017, 15831, 13597, 0, 13430, 13454, 13523, 13695, 13649, 13725, 13947, 13946, 13994, 0]

                    },
                    {
                        name: 'Income of living',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                color: '#f17a52',
                                label: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            emphasis: {
                                color: '#f17a52',
                            }
                        },
                        //data: [4500, 1000, 500, 700, 600, 800, 500, 400]
                        data: ['-', 1701, 186, 2234, '-', 167, '-', '-', '-', 46, 203, '-', '-']
                    }
                    ,
                    {
                        name: 'Cost of living',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                color: '#42A5F5',
                                label: {
                                    show: true,
                                    position: 'inside'
                                }
                            },
                            emphasis: {
                                color: '#42A5F5',
                            }
                        },
                        data: [17718, '-', '-', '-', 0, '-', 24, 69, 172, '-', 76, 222, 13994]
                        //data: [0, 0, 500, 0, 0, 0, 0, 0]
                    }
                ]
            });
        }

        if (aset_algeria_oil_production_profile_element) {

            // Initialize chart
            var aset_algeria_oil_production_profile = echarts.init(aset_algeria_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            aset_algeria_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                        if (params[2].value != '-') {
                            tar = params[2];
                        } else
                        if (params[3].value != '-') {
                           tar = params[3];
                        }else
                        if (params[4].value != '-') {
                            tar = params[4];
                        }
                        else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'MLN', 'OHD Field', 'EMK', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 22648, 22460, 23181, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [24074, '-', '-', '-',  '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', 721, '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 1426, 188, '-', '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 23181]
                    }
                ]
            });
        }

        if (aset_algeria_gas_production_profile_element) {

            // Initialize chart
            var aset_algeria_gas_production_profile = echarts.init(aset_algeria_gas_production_profile_element);


            //
            // Chart config
            //

            // Options
            aset_algeria_gas_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                        if (params[2].value != '-') {
                            tar = params[2];
                        } else
                        if (params[3].value != '-') {
                            tar = params[3];
                        } else
                        if (params[4].value != '-') {
                            tar = params[4];
                        }
                        else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
                    data: ['RKAP', 'MLN', 'OHD Field', 'EMK', 'Realisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate:90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 168, 165, 173, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [176, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', 8, '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 8, 3, '-', '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 173]
                    }
                ]
            });
        }

        if (asset_iraq_oil_production_profile_element) {

            // Initialize chart
            var asset_iraq_oil_production_profile = echarts.init(asset_iraq_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_iraq_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                        if (params[2].value != '-') {
                            tar = params[2];
                        } else
                        if (params[3].value != '-') {
                            tar = params[3];
                        } else
                        if (params[4].value != '-') {
                            tar = params[4];
                        }
                        else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
                    data: ['RKAP', 'WQ-1 Field', 'Realisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 48368, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [49243, '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 875, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', 48368]
                    }
                ]
            });
        }

        if (piep_oil_production_profile_ytd_element) {

            // Initialize chart
            var piep_oil_production_profile_ytd = echarts.init(piep_oil_production_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_oil_production_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                        if (params[2].value != '-') {
                            tar = params[2];
                        } else
                        if (params[3].value != '-') {
                            tar = params[3];
                        } else
                        if (params[4].value != '-') {
                            tar = params[4];
                        }
                        else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Asset Iraq', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 21311105.76, 21159824.76, 20860875.76, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [21356239.76, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 45134, 151281, 298949, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 20860875.76]
                    }
                ]
            });
        }

        if (piep_gas_production_profile_ytd_element) {

            // Initialize chart
            var piep_gas_production_profile_ytd = echarts.init(piep_gas_production_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_gas_production_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                        if (params[2].value != '-') {
                            tar = params[2];
                        } else
                        if (params[3].value != '-') {
                            tar = params[3];
                        } else
                        if (params[4].value != '-') {
                            tar = params[4];
                        }
                        else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 64704, 64043, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [62952, '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', 1752, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', '-', 661, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', 64043]
                    }
                ]
            });
        }

        if (piep_oil_equivalent_profile_ytd_element) {

            // Initialize chart
            var piep_oil_equivalent_profile_ytd = echarts.init(piep_oil_equivalent_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_oil_equivalent_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Asset Iraq', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 32479051.59, 32213760.59, 31914811.59, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [32221771.59, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', 257280, '-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', '-', 265291, 298949, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 31914811.59]
                    }
                ]
            });
        }

        // Change waterfall
        if (columns_change_waterfall_element) {

            // Initialize chart
            var columns_change_waterfall = echarts.init(columns_change_waterfall_element);


            //
            // Chart config
            //

            // Options
            columns_change_waterfall.setOption({

                // Define colors
                color: ['#03A9F4', '#f17a52'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Expenses', 'Income'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        }
                        else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'GOlok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 16017, 15831, 13597, 0, 13430, 13454, 13523, 13695, 13649, 13725, 13947, 13946, 13994]

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [17718, '-', '-', '-', 0, '-', 24, 69, 172, '-', 76, 222, '-', 13994]

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 1701, 186, 2234, '-', 167, '-', '-', '-', 46, '-','-', 1,'-']
                    }
                ]
            });
        }

        // Change waterfall
        if (asset_malaysia_oil_production_profile_element) {

            // Initialize chart
            var asset_malaysia_oil_production_profile = echarts.init(asset_malaysia_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_malaysia_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        }else
                        if (params[2].value != '-') {
                            tar = params[2];
                        }
                        else
                        if (params[3].value != '-') {
                            tar = params[3];
                        } else
                        if (params[4].value != '-') {
                            tar = params[4];
                        } else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'GOlok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 16017, 15831, 13597, 0, 13430, 13454, 13523, 13695, 13649, 13725, 13947, 13946, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [17718, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', 0, '-', 24, 69, 172, '-', 76, 222, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 1701, 186, 2234, '-', 167, '-', '-', '-', 46, '-', '-', 1, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 13994]
                    }
                ]
            });
        }
        
        // Change waterfall
        if (asset_malaysia_gas_production_profile_element) {

            // Initialize chart
            var asset_malaysia_gas_production_profile = echarts.init(asset_malaysia_gas_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_malaysia_gas_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        }
                        else
                        if (params[2].value != '-') {
                            tar = params[2];
                        }
                        else
                        if (params[3].value != '-') {
                            tar = params[3];
                        }
                        else
                        if (params[4].value != '-') {
                            tar = params[4];
                        }
                        else {
                            tar = params[0];
                        }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'GOlok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999',
                            rotate: 90
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 89, 88.9, 85.9, 87.7, 85.3, 85.5, 81.6, 82.5, 84.7, 84.3, 90.5, 89.8, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [89, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', 1.8, '-', 0.2, '-', 0.9, 2.2, '-', 6.2, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 0.0, 0.1, 3.0, '-', 2.4, '-', 3.9, '-', '-', 0.4, '-', 0.7, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 89.8]
                    }
                ]

            });
        }


        // Columns timeline
        if (columns_timeline_element) {

            // Initialize chart
            var columns_timeline = echarts.init(columns_timeline_element);


            //
            // Chart config
            //

            // Demo data
            var dataMap = {};
            dataMap.RKAP = ({
                2014:[16251.93,11307.28,24515.76],
                2013:[14113.58,9224.46,20394.26],
                2012:[12153.03,7521.85,17235.48],
                2011:[11115,6719.01,16011.97],
                2010:[9846.81,5252.76,13607.32]
            });
            dataMap.RCW = ({
                2014:[1074.93,411.46,918.02],
                2013:[1006.52,377.59,697.79],
                2012:[1062.47,308.73,612.4],
                2011:[844.59,227.88,513.81],
                2010:[821.5,183.44,467.97]
            });


            // Options
            columns_timeline.setOption({

                // Setup timeline
                timeline: {
                    axisType: 'category',
                    data: ['2010-01-01', '2011-01-01', '2012-01-01', '2013-01-01', '2014-01-01'],
                    left: 0,
                    right: 0,
                    bottom: 0,
                    label: {
                        normal: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 11
                        }
                    },
                    autoPlay: true,
                    playInterval: 3000
                },

                // Config
                options: [
                    {

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 10,
                            right: 10,
                            top: 35,
                            bottom: 60,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['RKAP','Realisasi Current Week'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            axisPointer: {
                                type: 'shadow',
                                shadowStyle: {
                                    color: 'rgba(0,0,0,0.025)'
                                }
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: ['Malaysia','Algeria','Iraq'],
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [
                            {
                                type: 'value',
                                name: 'BOEPD',
                                max: 53500, /*this yellow line*/
                                axisLabel: {
                                    color: '#333'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#999'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#eee'
                                    }
                                }
                            },
                            {
                                type: '',
                                name: '',
                                axisLabel: {
                                    color: '#fff'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                }
                            }
                        ],

                        // Add series
                        series: [
                            {
                                name: 'RKAP',
                                type: 'bar',
                                markLine: {
                                    symbol: ['arrow','none'],
                                    symbolSize: [4, 2],
                                    itemStyle: {
                                        normal: {
                                            lineStyle: {color: 'orange'},
                                            barBorderColor: 'orange',
                                            label: {
                                                position: 'left',
                                                formatter: function(params) {
                                                    return Math.round(params.value);
                                                },
                                                textStyle: {color: 'orange'}
                                            }
                                        }
                                    },
                                    data: [{type: 'average', name: 'Average'}]
                                },
                                data: dataMap.RKAP['2010']
                            },
                            {
                                name: 'Realisasi Current Week',
                                yAxisIndex: 1,
                                type: 'bar',
                                data: dataMap.RCW['2010']
                            }
                        ]
                    },

                    // 2011 data
                    {
                        series: [
                            {data: dataMap.RKAP['2011']},
                            {data: dataMap.RCW['2011']}
                        ]
                    },

                    // 2012 data
                    {
                        series: [
                            {data: dataMap.RKAP['2012']},
                            {data: dataMap.RCW['2012']}
                        ]
                    },

                    // 2013 data
                    {
                        series: [
                            {data: dataMap.RKAP['2013']},
                            {data: dataMap.RCW['2013']}
                        ]
                    },

                    // 2014 data
                    {
                        series: [
                            {data: dataMap.RKAP['2014']},
                            {data: dataMap.RCW['2014']}
                        ]
                    }
                ]
            });
        }

        // Columns timeline
        if (detail_asset_production_element) {

            // Initialize chart
            var detail_asset_production = echarts.init(detail_asset_production_element);


            //
            // Chart config
            //

            // Demo data
            var dataMapDetailAsset = {};
            dataMapDetailAsset.RKAP = ({
                2014: [16251.93, 11307.28, 24515.76],
                2013: [14113.58, 9224.46, 20394.26],
                2012: [12153.03, 7521.85, 17235.48],
                2011: [11115, 6719.01, 16011.97],
                2010: [9846.81, 5252.76, 13607.32]
            });
            dataMapDetailAsset.RCW = ({
                2014: [1074.93, 411.46, 918.02],
                2013: [1006.52, 377.59, 697.79],
                2012: [1062.47, 308.73, 612.4],
                2011: [844.59, 227.88, 513.81],
                2010: [821.5, 183.44, 467.97]
            });


            // Options
            detail_asset_production.setOption({

                // Setup timeline
                timeline: {
                    axisType: 'category',
                    data: ['2010-01-01', '2011-01-01', '2012-01-01', '2013-01-01', '2014-01-01'],
                    left: 0,
                    right: 0,
                    bottom: 0,
                    label: {
                        normal: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 11
                        }
                    },
                    autoPlay: true,
                    playInterval: 3000
                },

                // Config
                options: [
                    {

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 10,
                            right: 10,
                            top: 35,
                            bottom: 60,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['RKAP', 'Realisasi Current Week'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            axisPointer: {
                                type: 'shadow',
                                shadowStyle: {
                                    color: 'rgba(0,0,0,0.025)'
                                }
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: ['Malaysia', 'Algeria', 'Iraq'],
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [
                            {
                                type: 'value',
                                name: 'BOEPD',
                                max: 53500, /*this yellow line*/
                                axisLabel: {
                                    color: '#333'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#999'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#eee'
                                    }
                                }
                            },
                            {
                                type: '',
                                name: '',
                                axisLabel: {
                                    color: '#fff'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                }
                            }
                        ],

                        // Add series
                        series: [
                            {
                                name: 'RKAP',
                                type: 'bar',
                                markLine: {
                                    symbol: ['arrow', 'none'],
                                    symbolSize: [4, 2],
                                    itemStyle: {
                                        normal: {
                                            lineStyle: { color: 'orange' },
                                            barBorderColor: 'orange',
                                            label: {
                                                position: 'left',
                                                formatter: function (params) {
                                                    return Math.round(params.value);
                                                },
                                                textStyle: { color: 'orange' }
                                            }
                                        }
                                    },
                                    data: [{ type: 'average', name: 'Average' }]
                                },
                                data: dataMapDetailAsset.RKAP['2010']
                            },
                            {
                                name: 'Realisasi Current Week',
                                yAxisIndex: 1,
                                type: 'bar',
                                data: dataMapDetailAsset.RCW['2010']
                            }
                        ]
                    },

                    // 2011 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2011'] },
                            { data: dataMapDetailAsset.RCW['2011'] }
                        ]
                    },

                    // 2012 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2012'] },
                            { data: dataMapDetailAsset.RCW['2012'] }
                        ]
                    },

                    // 2013 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2013'] },
                            { data: dataMapDetailAsset.RCW['2013'] }
                        ]
                    },

                    // 2014 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2014'] },
                            { data: dataMapDetailAsset.RCW['2014'] }
                        ]
                    }
                ]
            });
        }


        //
        // Resize charts
        //

        // Resize function
        var triggerChartResize = function() {
            columns_basic_element && columns_basic.resize();
            columns_stacked_element && columns_stacked.resize();
            columns_thermometer_element && columns_thermometer.resize();
            columns_clustered_element && columns_clustered.resize();
            columns_compositive_waterfall_element && columns_compositive_waterfall.resize();
            columns_change_waterfall_element && columns_change_waterfall.resize();
            columns_timeline_element && columns_timeline.resize();
        };

        // On sidebar width change
        $(document).on('click', '.sidebar-control', function() {
            setTimeout(function () {
                triggerChartResize();
            }, 0);
        });

        // On window resize
        var resizeCharts;
        window.onresize = function () {
            clearTimeout(resizeCharts);
            resizeCharts = setTimeout(function () {
                triggerChartResize();
            }, 200);
        };
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _columnsWaterfallsExamples();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    EchartsColumnsWaterfalls.init();
});
