﻿/* ------------------------------------------------------------------------------
 *
 *  # Select2 selects
 *
 *  Specific JS code additions for form_select2.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var Select2Selects = function () {


    //
    // Setup module components
    //

    // Select2 examples
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Select with search
        $('.select-search').select2();

        //
        // Customize matched results
        //

        // Setup matcher
        function matchStart(term, text) {
            if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
                return true;
            }

            return false;
        }

        // Initialize
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $('.select-matched-customize').select2({
                minimumResultsForSearch: Infinity,
                placeholder: 'Select a State',
                matcher: oldMatcher(matchStart)
            });
        });

    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentSelect2();
        }
    }
}();

/* ------------------------------------------------------------------------------
 *
 *  # Checkboxes and radios
 *
 *  Demo JS code for form_checkboxes_radios.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var InputsCheckboxesRadios = function () {


    //
    // Setup components
    //

    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Default initialization
        $('.form-check-input-styled').uniform();


        //
        // Contextual colors
        //

        // Primary
        $('.form-check-input-styled-primary').uniform({
            wrapperClass: 'border-primary-600 text-primary-800'
        });

        // Danger
        $('.form-check-input-styled-danger').uniform({
            wrapperClass: 'border-danger-600 text-danger-800'
        });

        // Success
        $('.form-check-input-styled-success').uniform({
            wrapperClass: 'border-success-600 text-success-800'
        });

        // Warning
        $('.form-check-input-styled-warning').uniform({
            wrapperClass: 'border-warning-600 text-warning-800'
        });

        // Info
        $('.form-check-input-styled-info').uniform({
            wrapperClass: 'border-info-600 text-info-800'
        });

        // Custom color
        $('.form-check-input-styled-custom').uniform({
            wrapperClass: 'border-indigo-600 text-indigo-800'
        });
    };

    // Switchery
    var _componentSwitchery = function () {
        if (typeof Switchery == 'undefined') {
            console.warn('Warning - switchery.min.js is not loaded.');
            return;
        }

        // Initialize multiple switches
        var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });

        // Colored switches
        var primary = document.querySelector('.form-check-input-switchery-primary');
        var switchery = new Switchery(primary, { color: '#2196F3' });

        var danger = document.querySelector('.form-check-input-switchery-danger');
        var switchery = new Switchery(danger, { color: '#EF5350' });

        var warning = document.querySelector('.form-check-input-switchery-warning');
        var switchery = new Switchery(warning, { color: '#FF7043' });

        var info = document.querySelector('.form-check-input-switchery-info');
        var switchery = new Switchery(info, { color: '#00BCD4' });
    };

    // Bootstrap switch
    var _componentBootstrapSwitch = function () {
        if (!$().bootstrapSwitch) {
            console.warn('Warning - switch.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-check-input-switch').bootstrapSwitch();
    };

    //
    // Return objects assigned to module
    //

    return {
        initComponents: function () {
            _componentUniform();
            _componentSwitchery();
            _componentBootstrapSwitch();
        }
    }
}();


/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Pie and Donut charts
 *
 *  Demo JS code for echarts_pies_donuts.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var EchartsPiesDonuts = function () {


    //
    // Setup module components
    //

    // Pie and donut charts
    var _piesDonutsExamples = function () {
        if (typeof echarts == 'undefined') {
            console.warn('Warning - echarts.min.js is not loaded.');
            return;
        }

        // Define elements
        var piep_loss_production_opportunity_summaries_element = document.getElementById('piep-loss-production-opportunity-summaries');

        //
        // Charts configuration
        //

        // Basic pie chart
        if (piep_loss_production_opportunity_summaries_element) {

            // Initialize chart
            var piep_loss_production_opportunity_summaries = echarts.init(piep_loss_production_opportunity_summaries_element);


            //
            // Chart config
            //

            // Options
            piep_loss_production_opportunity_summaries.setOption({

                // Colors
                color: [
                    '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                ],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Add title
                title: {
                    text: '',
                    subtext: '',
                    left: 'center',
                    textStyle: {
                        fontSize: 17,
                        fontWeight: 500
                    },
                    subtextStyle: {
                        fontSize: 12
                    }
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'horizontal',
                    top: 'bottom',
                    left: 0,
                    data: ['IE', 'Opera', 'Safari', 'Firefox', 'Chrome'],
                    itemHeight: 8,
                    itemWidth: 8
                },

                // Add series
                series: [{
                    name: 'Browsers',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '40.5%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [
                        { value: 335, name: 'IE' },
                        { value: 310, name: 'Opera' },
                        { value: 234, name: 'Safari' },
                        { value: 135, name: 'Firefox' },
                        { value: 1548, name: 'Chrome' }
                    ]
                }]
            });
        }

        //
        // Resize charts
        //

        // Resize function
        var triggerChartResize = function () {
            piep_loss_production_opportunity_summaries_element && piep_loss_production_opportunity_summaries.resize();
        };

        // On sidebar width change
        $(document).on('click', '.sidebar-control', function () {
            setTimeout(function () {
                triggerChartResize();
            }, 0);
        });

        // On window resize
        var resizeCharts;
        window.onresize = function () {
            clearTimeout(resizeCharts);
            resizeCharts = setTimeout(function () {
                triggerChartResize();
            }, 200);
        };
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _piesDonutsExamples();
        }
    }
}();


/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Column and Waterfall charts
 *
 *  Demo JS code for echarts_columns_waterfalls.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var EchartsColumnsWaterfalls = function () {
    
    //
    // Setup module components
    //

    // Column and waterfall charts
    var _columnsWaterfallsExamples = function () {
        if (typeof echarts == 'undefined') {
            console.warn('Warning - echarts.min.js is not loaded.');
            return;
        }

        // Define elements
        var piep_gas_production_current_week_performance_element = document.getElementById('piep-gas-production-current-week-performance');
        var piep_oil_production_current_week_performance_element = document.getElementById('piep-oil-production-current-week-performance');
        var piep_total_production_current_week_performance_element = document.getElementById('piep-total-production-current-week-performance');

        var asset_malaysia_oil_production_profile_element = document.getElementById('asset_malaysia_oil_production_profile');
        var asset_malaysia_gas_production_profile_element = document.getElementById('asset-malaysia-gas-production-profile');

        var aset_algeria_oil_production_profile_element = document.getElementById('aset_algeria_oil_production_profile');
        var aset_algeria_gas_production_profile_element = document.getElementById('aset_algeria_gas_production_profile');

        var asset_iraq_oil_production_profile_element = document.getElementById('asset_iraq_oil_production_profile');

        var piep_oil_production_profile_ytd_element = document.getElementById('piep_oil_production_profile_ytd');
        var piep_gas_production_profile_ytd_element = document.getElementById('piep_gas_production_profile_ytd');
        var piep_oil_equivalent_profile_ytd_element = document.getElementById('piep_oil_equivalent_profile_ytd');

        var detail_asset_production_element = document.getElementById('detail-asset-production');

        var basecolorred = "#DC143C";
        var basecolorgreen = "#43A047";
        var basecolororange = "#F4511E";
        var basecoloryellow = "#f1c40f";
        var basecolorblue = "#1E88E5";
        var basecolorpurple = "#8e44ad";
        var basecolorpink = "#D81B60";
        var basecolorviolet = "#8E24AA";
        var basecolorteal = "#00897B";

        //
        // Charts configuration
        //

        // Stacked columns
        if (piep_gas_production_current_week_performance_element) {

            // Initialize chart
            var piep_gas_production_current_week_performance = echarts.init(piep_gas_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_gas_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Stacked columns
        if (piep_oil_production_current_week_performance_element) {

            // Initialize chart
            var piep_oil_production_current_week_performance = echarts.init(piep_oil_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_oil_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Stacked columns
        if (piep_total_production_current_week_performance_element) {

            // Initialize chart
            var piep_total_production_current_week_performance = echarts.init(piep_total_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_total_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        if (aset_algeria_oil_production_profile_element) {

            // Initialize chart
            var aset_algeria_oil_production_profile = echarts.init(aset_algeria_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            aset_algeria_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'MLN', 'OHD Field', 'EMK', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 22648, 22460, 23181, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [24074, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', 721, '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 1426, 188, '-', '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 23181]
                    }
                ]
            });
        }

        if (aset_algeria_gas_production_profile_element) {

            // Initialize chart
            var aset_algeria_gas_production_profile = echarts.init(aset_algeria_gas_production_profile_element);


            //
            // Chart config
            //

            // Options
            aset_algeria_gas_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
                    data: ['RKAP', 'MLN', 'OHD Field', 'EMK', 'Realisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 168, 165, 173, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [176, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', 8, '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 8, 3, '-', '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 173]
                    }
                ]
            });
        }

        if (asset_iraq_oil_production_profile_element) {

            // Initialize chart
            var asset_iraq_oil_production_profile = echarts.init(asset_iraq_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_iraq_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
                    data: ['RKAP', 'WQ-1 Field', 'Realisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 48368, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [49243, '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 875, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', 48368]
                    }
                ]
            });
        }

        if (piep_oil_production_profile_ytd_element) {

            // Initialize chart
            var piep_oil_production_profile_ytd = echarts.init(piep_oil_production_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_oil_production_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Asset Iraq', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 21311105.76, 21159824.76, 20860875.76, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [21356239.76, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 45134, 151281, 298949, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 20860875.76]
                    }
                ]
            });
        }

        if (piep_gas_production_profile_ytd_element) {

            // Initialize chart
            var piep_gas_production_profile_ytd = echarts.init(piep_gas_production_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_gas_production_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 64704, 64043, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [62952, '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', 1752, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', '-', 661, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', 64043]
                    }
                ]
            });
        }

        if (piep_oil_equivalent_profile_ytd_element) {

            // Initialize chart
            var piep_oil_equivalent_profile_ytd = echarts.init(piep_oil_equivalent_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_oil_equivalent_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Asset Iraq', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 32479051.59, 32213760.59, 31914811.59, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [32221771.59, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', 257280, '-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', '-', 265291, 298949, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 31914811.59]
                    }
                ]
            });
        }

        // Change waterfall
        if (asset_malaysia_oil_production_profile_element) {

            // Initialize chart
            var asset_malaysia_oil_production_profile = echarts.init(asset_malaysia_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_malaysia_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            }
                            else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    } else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'GOlok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 16017, 15831, 13597, 0, 13430, 13454, 13523, 13695, 13649, 13725, 13947, 13946, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [17718, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', 0, '-', 24, 69, 172, '-', 76, 222, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 1701, 186, 2234, '-', 167, '-', '-', '-', 46, '-', '-', 1, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 13994]
                    }
                ]
            });
        }

        // Change waterfall
        if (asset_malaysia_gas_production_profile_element) {

            // Initialize chart
            var asset_malaysia_gas_production_profile = echarts.init(asset_malaysia_gas_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_malaysia_gas_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        }
                        else
                            if (params[2].value != '-') {
                                tar = params[2];
                            }
                            else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                }
                                else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'GOlok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999',
                            rotate: 90
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 89, 88.9, 85.9, 87.7, 85.3, 85.5, 81.6, 82.5, 84.7, 84.3, 90.5, 89.8, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [89, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', 1.8, '-', 0.2, '-', 0.9, 2.2, '-', 6.2, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 0.0, 0.1, 3.0, '-', 2.4, '-', 3.9, '-', '-', 0.4, '-', 0.7, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 89.8]
                    }
                ]

            });
        }

        // Columns timeline
        if (detail_asset_production_element) {

            // Initialize chart
            var detail_asset_production = echarts.init(detail_asset_production_element);


            //
            // Chart config
            //

            // Demo data
            var dataMapDetailAsset = {};
            dataMapDetailAsset.RKAP = ({
                2014: [16251.93, 11307.28, 24515.76],
                2013: [14113.58, 9224.46, 20394.26],
                2012: [12153.03, 7521.85, 17235.48],
                2011: [11115, 6719.01, 16011.97],
                2010: [9846.81, 5252.76, 13607.32]
            });
            dataMapDetailAsset.RCW = ({
                2014: [1074.93, 411.46, 918.02],
                2013: [1006.52, 377.59, 697.79],
                2012: [1062.47, 308.73, 612.4],
                2011: [844.59, 227.88, 513.81],
                2010: [821.5, 183.44, 467.97]
            });


            // Options
            detail_asset_production.setOption({

                // Setup timeline
                timeline: {
                    axisType: 'category',
                    data: ['2010-01-01', '2011-01-01', '2012-01-01', '2013-01-01', '2014-01-01'],
                    left: 0,
                    right: 0,
                    bottom: 0,
                    label: {
                        normal: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 11
                        }
                    },
                    autoPlay: true,
                    playInterval: 3000
                },

                // Config
                options: [
                    {

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 10,
                            right: 10,
                            top: 35,
                            bottom: 60,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['RKAP', 'Realisasi Current Week'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            axisPointer: {
                                type: 'shadow',
                                shadowStyle: {
                                    color: 'rgba(0,0,0,0.025)'
                                }
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: ['Malaysia', 'Algeria', 'Iraq'],
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [
                            {
                                type: 'value',
                                name: 'BOEPD',
                                max: 53500, /*this yellow line*/
                                axisLabel: {
                                    color: '#333'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#999'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#eee'
                                    }
                                }
                            },
                            {
                                type: '',
                                name: '',
                                axisLabel: {
                                    color: '#fff'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                }
                            }
                        ],

                        // Add series
                        series: [
                            {
                                name: 'RKAP',
                                type: 'bar',
                                markLine: {
                                    symbol: ['arrow', 'none'],
                                    symbolSize: [4, 2],
                                    itemStyle: {
                                        normal: {
                                            lineStyle: { color: 'orange' },
                                            barBorderColor: 'orange',
                                            label: {
                                                position: 'left',
                                                formatter: function (params) {
                                                    return Math.round(params.value);
                                                },
                                                textStyle: { color: 'orange' }
                                            }
                                        }
                                    },
                                    data: [{ type: 'average', name: 'Average' }]
                                },
                                data: dataMapDetailAsset.RKAP['2010']
                            },
                            {
                                name: 'Realisasi Current Week',
                                yAxisIndex: 1,
                                type: 'bar',
                                data: dataMapDetailAsset.RCW['2010']
                            }
                        ]
                    },

                    // 2011 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2011'] },
                            { data: dataMapDetailAsset.RCW['2011'] }
                        ]
                    },

                    // 2012 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2012'] },
                            { data: dataMapDetailAsset.RCW['2012'] }
                        ]
                    },

                    // 2013 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2013'] },
                            { data: dataMapDetailAsset.RCW['2013'] }
                        ]
                    },

                    // 2014 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2014'] },
                            { data: dataMapDetailAsset.RCW['2014'] }
                        ]
                    }
                ]
            });
        }

        //
        // Resize charts
        //

        // Resize function
        var triggerChartResize = function () {
            piep_gas_production_current_week_performance_element && piep_gas_production_current_week_performance.resize();
            piep_oil_production_current_week_performance_element && piep_oil_production_current_week_performance.resize();
            piep_total_production_current_week_performance_element && piep_total_production_current_week_performance.resize();
            asset_malaysia_oil_production_profile_element && asset_malaysia_oil_production_profile.resize();
            asset_malaysia_gas_production_profile_element && asset_malaysia_gas_production_profile.resize();
            aset_algeria_oil_production_profile_element && aset_algeria_oil_production_profile.resize();
            aset_algeria_gas_production_profile_element && aset_algeria_gas_production_profile.resize();
            asset_iraq_oil_production_profile_element && asset_iraq_oil_production_profile.resize();
            piep_oil_production_profile_ytd_element && piep_oil_production_profile_ytd.resize();
            piep_gas_production_profile_ytd_element && piep_gas_production_profile_ytd.resize();
            piep_oil_equivalent_profile_ytd_element && piep_oil_equivalent_profile_ytd.resize();
            detail_asset_production_element && detail_asset_production.resize();
        };

        // On sidebar width change
        $(document).on('click', '.sidebar-control', function () {
            setTimeout(function () {
                triggerChartResize();
            }, 0);
        });

        // On window resize
        var resizeCharts;
        window.onresize = function () {
            clearTimeout(resizeCharts);
            resizeCharts = setTimeout(function () {
                triggerChartResize();
            }, 200);
        };
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _columnsWaterfallsExamples();
        }
    }
}();



/* ------------------------------------------------------------------------------
 *
 *  # C3.js - bars and pies
 *
 *  Demo JS code for c3_bars_pies.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var С3BarsPies = function () {


    //
    // Setup module components
    //

    // Chart
    var _barsPiesExamples = function () {
        if (typeof c3 == 'undefined') {
            console.warn('Warning - c3.min.js is not loaded.');
            return;
        }

        // Define charts elements
        var pie_chart_lpo_gk_field_element = document.getElementById('lpo-gk-field');
        var pie_chart_lpo_snp_field_element = document.getElementById('lpo-snp-field');
        var pie_chart_lpo_kikeh_field_element = document.getElementById('lpo-kikeh-field');
        var pie_chart_lpo_serendah_field_element = document.getElementById('lpo-serendah-field');
        var pie_chart_lpo_mln_field_element = document.getElementById('lpo-mln-field');
        var pie_chart_lpo_ohd_field_element = document.getElementById('lpo-ohd-field');
        var pie_chart_lpo_wq1_field_element = document.getElementById('lpo-wq1-field');

        var piep_oil_production_prognosis_element = document.getElementById('piep-oil-production-prognosis');
        var piep_gas_production_prognosis_element = document.getElementById('piep-gas-production-prognosis');
        var piep_oil_equivalent_production_prognosis_element = document.getElementById('piep-oil-equivalent-production-prognosis');

        // Pie chart
        if (pie_chart_lpo_gk_field_element) {

            // Generate chart
            var pie_chart_lpo_gk_field = c3.generate({
                bindto: pie_chart_lpo_gk_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#5ab1ef', '#FF9800', '#4CAF50', '#00BCD4', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 11905],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_gk_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_gk_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_gk_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_snp_field_element) {

            // Generate chart
            var pie_chart_lpo_snp_field = c3.generate({
                bindto: pie_chart_lpo_snp_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#FF9800', '#5ab1ef', '#4CAF50', '#00BCD4', '#F44336']
                },
                data: {
                    columns: [
                        ['Well', 1301.48],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_snp_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_snp_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_snp_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_kikeh_field_element) {

            // Generate chart
            var pie_chart_lpo_kikeh_field = c3.generate({
                bindto: pie_chart_lpo_kikeh_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#4CAF50', '#5ab1ef', '#FF9800', '#00BCD4', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 11905],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_kikeh_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_kikeh_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_kikeh_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_serendah_field_element) {

            // Generate chart
            var pie_chart_lpo_serendah_field = c3.generate({
                bindto: pie_chart_lpo_serendah_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 11905],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_serendah_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_serendah_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_serendah_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_mln_field_element) {

            // Generate chart
            var pie_chart_lpo_mln_field = c3.generate({
                bindto: pie_chart_lpo_mln_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['Well', 7881],
                        ['Reservoir', 2100],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_mln_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_mln_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_mln_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_ohd_field_element) {

            // Generate chart
            var pie_chart_lpo_ohd_field = c3.generate({
                bindto: pie_chart_lpo_ohd_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['Other', 1315],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_ohd_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_ohd_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_ohd_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_wq1_field_element) {

            // Generate chart
            var pie_chart_lpo_wq1_field = c3.generate({
                bindto: pie_chart_lpo_wq1_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 17426],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_wq1_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_wq1_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_wq1_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Combined chart
        if (piep_oil_production_prognosis_element) {

            // Generate chart
            var piep_oil_production_prognosis = c3.generate({
                bindto: piep_oil_production_prognosis_element,
                size: { height: 400 },
                color: {
                    pattern: ['#FF9800', '#F44336', '#009688', '#4CAF50', '#03A9F4', '#8BC34A']
                },
                data: {
                    columns: [
                        ['Prognosa', 30, 20, 50, 40, 60, 50],
                        //['data2', 200, 130, 90, 240, 130, 220],
                        ['RKAP', 30, 20, 50, 40, 60, 50],
                        //['data4', 200, 130, 90, 240, 130, 220],
                        //['data5', 130, 120, 150, 140, 160, 150],
                        //['data6', 90, 70, 20, 50, 60, 120],
                    ],
                    type: 'bar',
                    types: {
                        RKAP: 'spline',
                        data4: 'line',
                        data6: 'area',
                    },
                    groups: [
                        ['Prognosa', 'data2']
                    ]
                }
            });

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                piep_oil_production_prognosis.resize();
            });
        }

        // Combined chart
        if (piep_gas_production_prognosis_element) {

            // Generate chart
            var piep_gas_production_prognosis = c3.generate({
                bindto: piep_gas_production_prognosis_element,
                size: { height: 400 },
                color: {
                    pattern: ['#FF9800', '#F44336', '#009688', '#4CAF50', '#03A9F4', '#8BC34A']
                },
                data: {
                    columns: [
                        ['Prognosa', 200, 130, 90, 240, 130, 220],
                        //['data2', 200, 130, 90, 240, 130, 220],
                        ['RKAP', 200, 130, 90, 240, 130, 220],
                        //['data4', 200, 130, 90, 240, 130, 220],
                        //['data5', 130, 120, 150, 140, 160, 150],
                        //['data6', 90, 70, 20, 50, 60, 120],
                    ],
                    type: 'bar',
                    types: {
                        RKAP: 'spline',
                        data4: 'line',
                        data6: 'area',
                    },
                    groups: [
                        ['Prognosa', 'data2']
                    ]
                }
            });

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                piep_gas_production_prognosis.resize();
            });
        }

        // Combined chart
        if (piep_oil_equivalent_production_prognosis_element) {

            // Generate chart
            var piep_oil_equivalent_production_prognosis = c3.generate({
                bindto: piep_oil_equivalent_production_prognosis_element,
                size: { height: 400 },
                color: {
                    pattern: ['#FF9800', '#F44336', '#009688', '#4CAF50', '#03A9F4', '#8BC34A']
                },
                data: {
                    columns: [
                        ['Prognosa', 130, 120, 150, 140, 160, 150],
                        //['data2', 200, 130, 90, 240, 130, 220],
                        ['RKAP', 130, 120, 150, 140, 160, 150],
                        //['data4', 200, 130, 90, 240, 130, 220],
                        //['data5', 130, 120, 150, 140, 160, 150],
                        //['data6', 90, 70, 20, 50, 60, 120],
                    ],
                    type: 'bar',
                    types: {
                        RKAP: 'spline',
                        data4: 'line',
                        data6: 'area',
                    },
                    groups: [
                        ['Prognosa', 'data2']
                    ]
                }
            });

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                piep_oil_equivalent_production_prognosis.resize();
            });
        }
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _barsPiesExamples();
        }
    }
}();

// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    Select2Selects.init();
    EchartsPiesDonuts.init();
    InputsCheckboxesRadios.initComponents();
    С3BarsPies.init();
    EchartsColumnsWaterfalls.init();
});
