﻿/* ------------------------------------------------------------------------------
 *
 *  # Select2 selects
 *
 *  Specific JS code additions for form_select2.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var JqueryUiForms = function () {


    //
    // Setup module components
    //
    // Autiocomplete
    var _componentUiAutocomplete = function () {
        if (!$().autocomplete) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }

        // Add demo data
        var availableTags = [
            'ActionScript',
            'AppleScript',
            'Asp',
            'BASIC',
            'C',
            'C++',
            'Clojure',
            'COBOL',
            'ColdFusion',
            'Erlang',
            'Fortran',
            'Groovy',
            'Haskell',
            'Java',
            'JavaScript',
            'Lisp',
            'Perl',
            'PHP',
            'Python',
            'Ruby',
            'Scala',
            'Scheme'
        ];

        // Basic example
        $('#ac-basic').autocomplete({
            source: availableTags
        });


        //
        // Accent folding
        //

        // Add data
        var names = ['JÃ¶rn Zaefferer', 'Scott GonzÃ¡lez', 'John Resig'];

        // Map
        var accentMap = {
            'Ã¡': 'a',
            'Ã¶': 'o'
        };

        // Normalize
        var normalize = function (term) {
            var ret = '';
            for (var i = 0; i < term.length; i++) {
                ret += accentMap[term.charAt(i)] || term.charAt(i);
            }
            return ret;
        };

        // Init autocomplete
        $('#ac-folding').autocomplete({
            source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), 'i');
                response($.grep(names, function (value) {
                    value = value.label || value.value || value;
                    return matcher.test(value) || matcher.test(normalize(value));
                }));
            }
        });


        //
        // Categories
        //

        // Custom widget
        $.widget('custom.catcomplete', $.ui.autocomplete, {
            _create: function () {
                this._super();
                this.widget().menu('option', 'items', '> :not(.ui-autocomplete-category)');
            },
            _renderMenu: function (ul, items) {
                var that = this,
                    currentCategory = '';

                $.each(items, function (index, item) {
                    var li;
                    if (item.category != currentCategory) {
                        ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
                        currentCategory = item.category;
                    }
                    li = that._renderItemData(ul, item);
                    if (item.category) {
                        li.attr('aria-label', item.category + ' : ' + item.label);
                    }
                });
            }
        });

        // Demo data
        var data = [
            { label: 'anders', category: '' },
            { label: 'andreas', category: '' },
            { label: 'antal', category: '' },
            { label: 'annhhx10', category: 'Products' },
            { label: 'annk K12', category: 'Products' },
            { label: 'annttop C13', category: 'Products' },
            { label: 'anders andersson', category: 'People' },
            { label: 'andreas andersson', category: 'People' },
            { label: 'andreas johnson', category: 'People' }
        ];

        // Initialize autocomplete
        $('#ac-categories').catcomplete({
            delay: 0,
            source: data
        });


        //
        // Custom data and display
        //

        // Demo data
        var projects = [
            {
                label: 'Limitless',
                desc: 'responsive web application kit'
            },
            {
                label: 'Londinium',
                desc: 'responsive bootstrap 3 admin template'
            },
            {
                label: 'It\'s Brain',
                desc: 'Bootstrap based premium admin template'
            }
        ];

        // Initialize autocomplete
        $('#ac-custom').autocomplete({
            minLength: 0,
            source: projects,
            focus: function (event, ui) {
                $('#ac-custom').val(ui.item.label);
                return false;
            },
            select: function (event, ui) {
                $('#ac-custom').val(ui.item.label);
                return false;
            }
        })
            .autocomplete('instance')._renderItem = function (ul, item) {
                return $('<li>').append('<span class="font-weight-semibold pb-0">' + item.label + '</span>' + '<div class="text-muted font-size-sm pt-0">' + item.desc + '</div>').appendTo(ul);
            };


        //
        // Custom data and display
        //

        // Split values
        function split(val) {
            return val.split(/,\s*/);
        }

        // Extract the last term
        function extractLast(term) {
            return split(term).pop();
        }

        // Configure and initialize
        $('#ac-multiple').bind('keydown', function (event) {
            if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete('instance').menu.active) {
                event.preventDefault();
            }
        })
            .autocomplete({
                minLength: 0,
                source: function (request, response) {

                    // Delegate back to autocomplete, but extract the last term
                    response($.ui.autocomplete.filter(
                        availableTags, extractLast(request.term)));
                },
                focus: function () {

                    // Prevent value inserted on focus
                    return false;
                },
                select: function (event, ui) {
                    var terms = split(this.value);

                    // Remove the current input
                    terms.pop();

                    // Add the selected item
                    terms.push(ui.item.value);

                    // Add placeholder to get the comma-and-space at the end
                    terms.push('');
                    this.value = terms.join('', '');
                    return false;
                }
            });


        //
        // Remote data
        //

        // Remote data
        $('#ac-remote').autocomplete({
            minLength: 2,
            source: '../../../../global_assets/demo_data/jquery_ui/autocomplete.php',
            search: function () {
                $(this).parent().addClass('ui-autocomplete-processing');
            },
            open: function () {
                $(this).parent().removeClass('ui-autocomplete-processing');
            }
        });

        // Remote data with caching
        var cache = {};
        $('#ac-caching').autocomplete({
            minLength: 2,
            source: function (request, response) {
                var term = request.term;
                if (term in cache) {
                    response(cache[term]);
                    return;
                }

                $.getJSON('../../../../global_assets/demo_data/jquery_ui/autocomplete.php', request, function (data, status, xhr) {
                    cache[term] = data;
                    response(data);
                });
            },
            search: function () {
                $(this).parent().addClass('ui-autocomplete-processing');
            },
            open: function () {
                $(this).parent().removeClass('ui-autocomplete-processing');
            }
        });


        //
        // Combo box
        //

        // Configure custom widget
        $.widget('custom.combobox', {
            _create: function () {
                this.wrapper = $('<div>').addClass('custom-combobox input-group').insertAfter(this.element);
                this.element.hide();
                this._createAutocomplete();
                this._createShowAllButton();
            },
            _createAutocomplete: function () {
                var selected = this.element.children(':selected'),
                    value = selected.val() ? selected.text() : '';

                var input = this.input = $('<input>')
                    .appendTo(this.wrapper)
                    .val(value)
                    .attr('title', '')
                    .attr('placeholder', 'Search')
                    .addClass('form-control')
                    .autocomplete({
                        delay: 0,
                        minLength: 0,
                        source: $.proxy(this, '_source')
                    });

                this._on(this.input, {
                    autocompleteselect: function (event, ui) {
                        ui.item.option.selected = true;
                        this._trigger('select', event, {
                            item: ui.item.option
                        });
                    },

                    autocompletechange: '_removeIfInvalid'
                });
            },
            _createShowAllButton: function () {
                var input = this.input,
                    wasOpen = false;

                // Append fonr control icon
                this.wrapper.append('<div class="form-control-feedback"><i class="icon-search4 font-size-base"></i></div>');

                // Add input group button
                var wrapper2 = $('<span>').attr('class', 'input-group-append').appendTo(this.wrapper);


                // Link
                $('<a>')
                    .attr('tabIndex', -1)
                    .appendTo(wrapper2)
                    .button({
                        icons: {
                            primary: 'icon-arrow-down12'
                        },
                        text: false
                    })
                    .removeClass('')
                    .on('mousedown', function () {
                        wasOpen = input.autocomplete('widget').is(':visible');
                    })
                    .on('click', function () {
                        input.focus();

                        // Close if already visible
                        if (wasOpen) {
                            return;
                        }

                        // Pass empty string as value to search for, displaying all results
                        input.autocomplete('search', '');
                    });
            },
            _source: function (request, response) {
                var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), 'i');
                response(this.element.children('option').map(function () {
                    var text = $(this).text();
                    if (this.value && (!request.term || matcher.test(text)))
                        return {
                            label: text,
                            value: text,
                            option: this
                        };
                })
                );
            },
            _removeIfInvalid: function (event, ui) {

                // Selected an item, nothing to do
                if (ui.item) {
                    return;
                }

                // Search for a match (case-insensitive)
                var value = this.input.val(),
                    valueLowerCase = value.toLowerCase(),
                    valid = false;

                this.element.children('option').each(function () {
                    if ($(this).text().toLowerCase() === valueLowerCase) {
                        this.selected = valid = true;
                        return false;
                    }
                });

                // Found a match, nothing to do
                if (valid) {
                    return;
                }

                // Remove invalid value
                this.input.val('').attr('title', value + ' didn\'t match any item');

                this.element.val('');
                this._delay(function () {
                    this.input.tooltip('close').attr('title', '');
                }, 2500);
                this.input.autocomplete('instance').term = '';
            },
            _destroy: function () {
                this.wrapper.remove();
                this.element.show();
            }
        });

        // Initialize
        $('#ac-combo').combobox();
    };
    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentUiAutocomplete();
        }
    }
}();

var Select2Selects = function () {


    //
    // Setup module components
    //

    // Select2 examples
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Select with search
        $('.select-search').select2();

        //
        // Customize matched results
        //

        // Setup matcher
        function matchStart(term, text) {
            if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
                return true;
            }

            return false;
        }

        // Initialize
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $('.select-matched-customize').select2({
                minimumResultsForSearch: Infinity,
                placeholder: 'Select a State',
                matcher: oldMatcher(matchStart)
            });
        });

    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentSelect2();
        }
    }
}();

var BasicAlert = function () {

    var _BasicAlerts = function () {
        //default
        $("#default-alert").hide();
        $("#btnDefault").click(function showAlert() {
            $("#default-alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#default-alert").slideUp(500);
            });
        });

        //primary
        $("#primary-alert").hide();
        $("#btnPrimary").click(function showAlert() {
            $("#primary-alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#primary-alert").slideUp(500);
            });
        });

        //success
        $("#success-alert").hide();
        $("#btnSuccess").click(function showAlert() {
            $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#success-alert").slideUp(500);
            });
        });

        //warning
        $("#warning-alert").hide();
        $("#btnWarning").click(function showAlert() {
            $("#warning-alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#warning-alert").slideUp(500);
            });
        });

        //Danger
        $("#danger-alert").hide();
        $("#btnDanger").click(function showAlert() {
            $("#danger-alert").fadeTo(2000, 500).slideUp(500, function () {
                $("#danger-alert").slideUp(500);
            });
        });
    };

    return {
        init: function () {
            _BasicAlerts();
        }
    }
}();

/* ------------------------------------------------------------------------------
 *
 *  # Checkboxes and radios
 *
 *  Demo JS code for form_checkboxes_radios.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var InputsCheckboxesRadios = function () {


    //
    // Setup components
    //

    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Default initialization
        $('.form-check-input-styled').uniform();


        //
        // Contextual colors
        //

        // Primary
        $('.form-check-input-styled-primary').uniform({
            wrapperClass: 'border-primary-600 text-primary-800'
        });

        // Danger
        $('.form-check-input-styled-danger').uniform({
            wrapperClass: 'border-danger-600 text-danger-800'
        });

        // Success
        $('.form-check-input-styled-success').uniform({
            wrapperClass: 'border-success-600 text-success-800'
        });

        // Warning
        $('.form-check-input-styled-warning').uniform({
            wrapperClass: 'border-warning-600 text-warning-800'
        });

        // Info
        $('.form-check-input-styled-info').uniform({
            wrapperClass: 'border-info-600 text-info-800'
        });

        // Custom color
        $('.form-check-input-styled-custom').uniform({
            wrapperClass: 'border-indigo-600 text-indigo-800'
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        initComponents: function () {
            _componentUniform();
        }
    }
}();


/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Pie and Donut charts
 *
 *  Demo JS code for echarts_pies_donuts.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var EchartsPiesDonuts = function () {


    //
    // Setup module components
    //

    // Pie and donut charts
    var _piesDonutsExamples = function () {
        if (typeof echarts == 'undefined') {
            console.warn('Warning - echarts.min.js is not loaded.');
            return;
        }

        // Define elements
        var piep_loss_production_opportunity_summaries_element = document.getElementById('piep-loss-production-opportunity-summaries');

        //
        // Charts configuration
        //

        // Basic pie chart
        if (piep_loss_production_opportunity_summaries_element) {

            // Initialize chart
            var piep_loss_production_opportunity_summaries = echarts.init(piep_loss_production_opportunity_summaries_element);


            //
            // Chart config
            //

            // Options
            piep_loss_production_opportunity_summaries.setOption({

                // Colors
                color: [
                    '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                ],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Add title
                title: {
                    text: '',
                    subtext: '',
                    left: 'center',
                    textStyle: {
                        fontSize: 17,
                        fontWeight: 500
                    },
                    subtextStyle: {
                        fontSize: 12
                    }
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'horizontal',
                    top: 'bottom',
                    left: 0,
                    data: ['IE', 'Opera', 'Safari', 'Firefox', 'Chrome'],
                    itemHeight: 8,
                    itemWidth: 8
                },

                // Add series
                series: [{
                    name: 'Browsers',
                    type: 'pie',
                    radius: '70%',
                    center: ['50%', '40.5%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [
                        { value: 335, name: 'IE' },
                        { value: 310, name: 'Opera' },
                        { value: 234, name: 'Safari' },
                        { value: 135, name: 'Firefox' },
                        { value: 1548, name: 'Chrome' }
                    ]
                }]
            });
        }

        //
        // Resize charts
        //

        // Resize function
        var triggerChartResize = function () {
            piep_loss_production_opportunity_summaries_element && piep_loss_production_opportunity_summaries.resize();
        };

        // On sidebar width change
        $(document).on('click', '.sidebar-control', function () {
            setTimeout(function () {
                triggerChartResize();
            }, 0);
        });

        // On window resize
        var resizeCharts;
        window.onresize = function () {
            clearTimeout(resizeCharts);
            resizeCharts = setTimeout(function () {
                triggerChartResize();
            }, 200);
        };
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _piesDonutsExamples();
        }
    }
}();


/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Column and Waterfall charts
 *
 *  Demo JS code for echarts_columns_waterfalls.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var EchartsColumnsWaterfalls = function () {

    //
    // Setup module components
    //

    // Column and waterfall charts
    var _columnsWaterfallsExamples = function () {
        if (typeof echarts == 'undefined') {
            console.warn('Warning - echarts.min.js is not loaded.');
            return;
        }

        // Define elements
        var piep_gas_production_current_week_performance_element = document.getElementById('piep-gas-production-current-week-performance');
        var piep_oil_production_current_week_performance_element = document.getElementById('piep-oil-production-current-week-performance');
        var piep_total_production_current_week_performance_element = document.getElementById('piep-total-production-current-week-performance');

        var asset_malaysia_oil_production_profile_element = document.getElementById('asset_malaysia_oil_production_profile');
        var asset_malaysia_gas_production_profile_element = document.getElementById('asset-malaysia-gas-production-profile');

        var aset_algeria_oil_production_profile_element = document.getElementById('aset_algeria_oil_production_profile');
        var aset_algeria_gas_production_profile_element = document.getElementById('aset_algeria_gas_production_profile');

        var asset_iraq_oil_production_profile_element = document.getElementById('asset_iraq_oil_production_profile');

        var piep_oil_production_profile_ytd_element = document.getElementById('piep_oil_production_profile_ytd');
        var piep_gas_production_profile_ytd_element = document.getElementById('piep_gas_production_profile_ytd');
        var piep_oil_equivalent_profile_ytd_element = document.getElementById('piep_oil_equivalent_profile_ytd');

        var detail_asset_production_element = document.getElementById('detail-asset-production');

        var basecolorred = "#DC143C";
        var basecolorgreen = "#43A047";
        var basecolororange = "#F4511E";
        var basecoloryellow = "#f1c40f";
        var basecolorblue = "#1E88E5";
        var basecolorpurple = "#8e44ad";
        var basecolorpink = "#D81B60";
        var basecolorviolet = "#8E24AA";
        var basecolorteal = "#00897B";

        //
        // Charts configuration
        //

        // Stacked columns
        if (piep_gas_production_current_week_performance_element) {

            // Initialize chart
            var piep_gas_production_current_week_performance = echarts.init(piep_gas_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_gas_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Stacked columns
        if (piep_oil_production_current_week_performance_element) {

            // Initialize chart
            var piep_oil_production_current_week_performance = echarts.init(piep_oil_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_oil_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        // Stacked columns
        if (piep_total_production_current_week_performance_element) {

            // Initialize chart
            var piep_total_production_current_week_performance = echarts.init(piep_total_production_current_week_performance_element);


            //
            // Chart config
            //

            // Options
            piep_total_production_current_week_performance.setOption({

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Direct', 'Email', 'Prints', 'Videos', 'Television', 'Yahoo', 'Google', 'Bing', 'Other'],
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Direct',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Email',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Prints',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Videos',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Television',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570]
                    },
                    {
                        name: 'Yahoo',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120]
                    },
                    {
                        name: 'Google',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220]
                    },
                    {
                        name: 'Bing',
                        type: 'bar',
                        stack: 'Television',
                        data: [60, 72, 71, 74, 190, 130, 110]
                    },
                    {
                        name: 'Other',
                        type: 'bar',
                        stack: 'Television',
                        data: [62, 82, 91, 84, 109, 110, 120]
                    }
                ]
            });
        }

        if (aset_algeria_oil_production_profile_element) {

            // Initialize chart
            var aset_algeria_oil_production_profile = echarts.init(aset_algeria_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            aset_algeria_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'MLN', 'OHD Field', 'EMK', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 22648, 22460, 23181, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [24074, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', 721, '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 1426, 188, '-', '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 23181]
                    }
                ]
            });
        }

        if (aset_algeria_gas_production_profile_element) {

            // Initialize chart
            var aset_algeria_gas_production_profile = echarts.init(aset_algeria_gas_production_profile_element);


            //
            // Chart config
            //

            // Options
            aset_algeria_gas_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
                    data: ['RKAP', 'MLN', 'OHD Field', 'EMK', 'Realisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 168, 165, 173, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [176, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', 8, '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 8, 3, '-', '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 173]
                    }
                ]
            });
        }

        if (asset_iraq_oil_production_profile_element) {

            // Initialize chart
            var asset_iraq_oil_production_profile = echarts.init(asset_iraq_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_iraq_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['Total cost', 'Rent', 'Utilities', 'Transport', 'Meals', 'Commodity', 'Taxes', 'Travel'],
                    data: ['RKAP', 'WQ-1 Field', 'Realisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 48368, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [49243, '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 875, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', 48368]
                    }
                ]
            });
        }

        if (piep_oil_production_profile_ytd_element) {

            // Initialize chart
            var piep_oil_production_profile_ytd = echarts.init(piep_oil_production_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_oil_production_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Asset Iraq', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 21311105.76, 21159824.76, 20860875.76, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [21356239.76, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 45134, 151281, 298949, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 20860875.76]
                    }
                ]
            });
        }

        if (piep_gas_production_profile_ytd_element) {

            // Initialize chart
            var piep_gas_production_profile_ytd = echarts.init(piep_gas_production_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_gas_production_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 64704, 64043, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [62952, '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', 1752, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', '-', 661, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', 64043]
                    }
                ]
            });
        }

        if (piep_oil_equivalent_profile_ytd_element) {

            // Initialize chart
            var piep_oil_equivalent_profile_ytd = echarts.init(piep_oil_equivalent_profile_ytd_element);


            //
            // Chart config
            //

            // Options
            piep_oil_equivalent_profile_ytd.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            } else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['RKAP', 'Asset Malaysia', 'Asset Algeria ASSET', 'Asset Iraq', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 45
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 32479051.59, 32213760.59, 31914811.59, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [32221771.59, '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', 257280, '-', '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', '-', 265291, 298949, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', 31914811.59]
                    }
                ]
            });
        }

        // Change waterfall
        if (asset_malaysia_oil_production_profile_element) {

            // Initialize chart
            var asset_malaysia_oil_production_profile = echarts.init(asset_malaysia_oil_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_malaysia_oil_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        } else
                            if (params[2].value != '-') {
                                tar = params[2];
                            }
                            else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                } else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    } else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'GOlok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 16017, 15831, 13597, 0, 13430, 13454, 13523, 13695, 13649, 13725, 13947, 13946, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [17718, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', 0, '-', 24, 69, 172, '-', 76, 222, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 1701, 186, 2234, '-', 167, '-', '-', '-', 46, '-', '-', 1, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 13994]
                    }
                ]
            });
        }

        // Change waterfall
        if (asset_malaysia_gas_production_profile_element) {

            // Initialize chart
            var asset_malaysia_gas_production_profile = echarts.init(asset_malaysia_gas_production_profile_element);


            //
            // Chart config
            //

            // Options
            asset_malaysia_gas_production_profile.setOption({

                // Define colors
                color: [basecolorblue, basecolorgreen, basecolorred],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 10,
                    right: 10,
                    top: 35,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['RKAP', 'Expenses', 'Income', 'Realisasi'],
                    itemHeight: 8,
                    itemGap: 20,
                    textStyle: {
                        padding: [0, 5]
                    }
                },

                // Tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    },
                    formatter: function (params) {
                        var tar;
                        if (params[1].value != '-') {
                            tar = params[1];
                        }
                        else
                            if (params[2].value != '-') {
                                tar = params[2];
                            }
                            else
                                if (params[3].value != '-') {
                                    tar = params[3];
                                }
                                else
                                    if (params[4].value != '-') {
                                        tar = params[4];
                                    }
                                    else {
                                        tar = params[0];
                                    }
                        return tar.name + '<br/>' + tar.seriesName + ': ' + tar.value;
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    //data: ['January','February','March','April','May','June','July','August','September','October','November','December'],
                    data: ['RKAP', 'GK', 'SNP', 'Kikeh', 'West Patricia', 'Serendah', 'Patricia', 'GOlok(C)', 'Merapuh(C)', 'Belum(C)', 'Permas', 'South Acis', 'Serampang', 'Ralisasi'],

                    axisLabel: {
                        color: '#333',
                        rotate: 90
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999',
                            rotate: 90
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Aid',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            },
                            emphasis: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        //data: [0, 900, 1245, 1530, 1376, 1376, 1511, 1689, 1856, 1495, 1292, 992]
                        data: [0, 89, 88.9, 85.9, 87.7, 85.3, 85.5, 81.6, 82.5, 84.7, 84.3, 90.5, 89.8, 0]

                    },
                    {
                        name: 'RKAP',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: [89, '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-']

                    },
                    {
                        name: 'Income',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        //data: [900, 345, 393, '-', '-', 135, 178, 286, '-', '-', '-']
                        data: ['-', '-', '-', '-', 1.8, '-', 0.2, '-', 0.9, 2.2, '-', 6.2, '-', '-']

                    },
                    {
                        name: 'Expenses',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'bottom'
                                }
                            }
                        },
                        data: ['-', 0.0, 0.1, 3.0, '-', 2.4, '-', 3.9, '-', '-', 0.4, '-', 0.7, '-']
                    },
                    {
                        name: 'Realisasi',
                        type: 'bar',
                        stack: 'Total',
                        itemStyle: {
                            normal: {
                                barBorderRadius: 3,
                                label: {
                                    show: true,
                                    position: 'top'
                                }
                            }
                        },
                        data: ['-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 89.8]
                    }
                ]

            });
        }

        // Columns timeline
        if (detail_asset_production_element) {

            // Initialize chart
            var detail_asset_production = echarts.init(detail_asset_production_element);


            //
            // Chart config
            //

            // Demo data
            var dataMapDetailAsset = {};
            dataMapDetailAsset.RKAP = ({
                2014: [16251.93, 11307.28, 24515.76],
                2013: [14113.58, 9224.46, 20394.26],
                2012: [12153.03, 7521.85, 17235.48],
                2011: [11115, 6719.01, 16011.97],
                2010: [9846.81, 5252.76, 13607.32]
            });
            dataMapDetailAsset.RCW = ({
                2014: [1074.93, 411.46, 918.02],
                2013: [1006.52, 377.59, 697.79],
                2012: [1062.47, 308.73, 612.4],
                2011: [844.59, 227.88, 513.81],
                2010: [821.5, 183.44, 467.97]
            });


            // Options
            detail_asset_production.setOption({

                // Setup timeline
                timeline: {
                    axisType: 'category',
                    data: ['2010-01-01', '2011-01-01', '2012-01-01', '2013-01-01', '2014-01-01'],
                    left: 0,
                    right: 0,
                    bottom: 0,
                    label: {
                        normal: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 11
                        }
                    },
                    autoPlay: true,
                    playInterval: 3000
                },

                // Config
                options: [
                    {

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 10,
                            right: 10,
                            top: 35,
                            bottom: 60,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['RKAP', 'Realisasi Current Week'],
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            axisPointer: {
                                type: 'shadow',
                                shadowStyle: {
                                    color: 'rgba(0,0,0,0.025)'
                                }
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            data: ['Malaysia', 'Algeria', 'Iraq'],
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.015)']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [
                            {
                                type: 'value',
                                name: 'BOEPD',
                                max: 53500, /*this yellow line*/
                                axisLabel: {
                                    color: '#333'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#999'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#eee'
                                    }
                                }
                            },
                            {
                                type: '',
                                name: '',
                                axisLabel: {
                                    color: '#fff'
                                },
                                axisLine: {
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                },
                                splitLine: {
                                    show: true,
                                    lineStyle: {
                                        color: '#fff'
                                    }
                                }
                            }
                        ],

                        // Add series
                        series: [
                            {
                                name: 'RKAP',
                                type: 'bar',
                                markLine: {
                                    symbol: ['arrow', 'none'],
                                    symbolSize: [4, 2],
                                    itemStyle: {
                                        normal: {
                                            lineStyle: { color: 'orange' },
                                            barBorderColor: 'orange',
                                            label: {
                                                position: 'left',
                                                formatter: function (params) {
                                                    return Math.round(params.value);
                                                },
                                                textStyle: { color: 'orange' }
                                            }
                                        }
                                    },
                                    data: [{ type: 'average', name: 'Average' }]
                                },
                                data: dataMapDetailAsset.RKAP['2010']
                            },
                            {
                                name: 'Realisasi Current Week',
                                yAxisIndex: 1,
                                type: 'bar',
                                data: dataMapDetailAsset.RCW['2010']
                            }
                        ]
                    },

                    // 2011 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2011'] },
                            { data: dataMapDetailAsset.RCW['2011'] }
                        ]
                    },

                    // 2012 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2012'] },
                            { data: dataMapDetailAsset.RCW['2012'] }
                        ]
                    },

                    // 2013 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2013'] },
                            { data: dataMapDetailAsset.RCW['2013'] }
                        ]
                    },

                    // 2014 data
                    {
                        series: [
                            { data: dataMapDetailAsset.RKAP['2014'] },
                            { data: dataMapDetailAsset.RCW['2014'] }
                        ]
                    }
                ]
            });
        }

        //
        // Resize charts
        //

        // Resize function
        var triggerChartResize = function () {
            piep_gas_production_current_week_performance_element && piep_gas_production_current_week_performance.resize();
            piep_oil_production_current_week_performance_element && piep_oil_production_current_week_performance.resize();
            piep_total_production_current_week_performance_element && piep_total_production_current_week_performance.resize();
            asset_malaysia_oil_production_profile_element && asset_malaysia_oil_production_profile.resize();
            asset_malaysia_gas_production_profile_element && asset_malaysia_gas_production_profile.resize();
            aset_algeria_oil_production_profile_element && aset_algeria_oil_production_profile.resize();
            aset_algeria_gas_production_profile_element && aset_algeria_gas_production_profile.resize();
            asset_iraq_oil_production_profile_element && asset_iraq_oil_production_profile.resize();
            piep_oil_production_profile_ytd_element && piep_oil_production_profile_ytd.resize();
            piep_gas_production_profile_ytd_element && piep_gas_production_profile_ytd.resize();
            piep_oil_equivalent_profile_ytd_element && piep_oil_equivalent_profile_ytd.resize();
            detail_asset_production_element && detail_asset_production.resize();
        };

        // On sidebar width change
        $(document).on('click', '.sidebar-control', function () {
            setTimeout(function () {
                triggerChartResize();
            }, 0);
        });

        // On window resize
        var resizeCharts;
        window.onresize = function () {
            clearTimeout(resizeCharts);
            resizeCharts = setTimeout(function () {
                triggerChartResize();
            }, 200);
        };
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _columnsWaterfallsExamples();
        }
    }
}();



/* ------------------------------------------------------------------------------
 *
 *  # C3.js - bars and pies
 *
 *  Demo JS code for c3_bars_pies.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var С3BarsPies = function () {


    //
    // Setup module components
    //

    // Chart
    var _barsPiesExamples = function () {
        if (typeof c3 == 'undefined') {
            console.warn('Warning - c3.min.js is not loaded.');
            return;
        }

        // Define charts elements
        var pie_chart_lpo_gk_field_element = document.getElementById('lpo-gk-field');
        var pie_chart_lpo_snp_field_element = document.getElementById('lpo-snp-field');
        var pie_chart_lpo_kikeh_field_element = document.getElementById('lpo-kikeh-field');
        var pie_chart_lpo_serendah_field_element = document.getElementById('lpo-serendah-field');
        var pie_chart_lpo_mln_field_element = document.getElementById('lpo-mln-field');
        var pie_chart_lpo_ohd_field_element = document.getElementById('lpo-ohd-field');
        var pie_chart_lpo_wq1_field_element = document.getElementById('lpo-wq1-field');

        var piep_oil_production_prognosis_element = document.getElementById('piep-oil-production-prognosis');
        var piep_gas_production_prognosis_element = document.getElementById('piep-gas-production-prognosis');
        var piep_oil_equivalent_production_prognosis_element = document.getElementById('piep-oil-equivalent-production-prognosis');

        // Pie chart
        if (pie_chart_lpo_gk_field_element) {

            // Generate chart
            var pie_chart_lpo_gk_field = c3.generate({
                bindto: pie_chart_lpo_gk_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#5ab1ef', '#FF9800', '#4CAF50', '#00BCD4', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 11905],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_gk_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_gk_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_gk_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_snp_field_element) {

            // Generate chart
            var pie_chart_lpo_snp_field = c3.generate({
                bindto: pie_chart_lpo_snp_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#FF9800', '#5ab1ef', '#4CAF50', '#00BCD4', '#F44336']
                },
                data: {
                    columns: [
                        ['Well', 1301.48],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_snp_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_snp_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_snp_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_kikeh_field_element) {

            // Generate chart
            var pie_chart_lpo_kikeh_field = c3.generate({
                bindto: pie_chart_lpo_kikeh_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#4CAF50', '#5ab1ef', '#FF9800', '#00BCD4', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 11905],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_kikeh_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_kikeh_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_kikeh_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_serendah_field_element) {

            // Generate chart
            var pie_chart_lpo_serendah_field = c3.generate({
                bindto: pie_chart_lpo_serendah_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 11905],
                        //['data2', 20],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_serendah_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_serendah_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_serendah_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_mln_field_element) {

            // Generate chart
            var pie_chart_lpo_mln_field = c3.generate({
                bindto: pie_chart_lpo_mln_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['Well', 7881],
                        ['Reservoir', 2100],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_mln_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_mln_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_mln_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_ohd_field_element) {

            // Generate chart
            var pie_chart_lpo_ohd_field = c3.generate({
                bindto: pie_chart_lpo_ohd_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['Other', 1315],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_ohd_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_ohd_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_ohd_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Pie chart
        if (pie_chart_lpo_wq1_field_element) {

            // Generate chart
            var pie_chart_lpo_wq1_field = c3.generate({
                bindto: pie_chart_lpo_wq1_field_element,
                size: { width: 250 },
                color: {
                    pattern: ['#00BCD4', '#5ab1ef', '#FF9800', '#4CAF50', '#F44336']
                },
                data: {
                    columns: [
                        ['SF', 17426],
                    ],
                    type: 'pie'
                }
            });

            // Change data
            //setTimeout(function () {
            //    pie_chart_lpo_wq1_field.load({
            //        columns: [
            //            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
            //            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
            //            ["virginica", 2.5, 1.9, 2.1, 1.8, 2.2, 2.1, 1.7, 1.8, 1.8, 2.5, 2.0, 1.9, 2.1, 2.0, 2.4, 2.3, 1.8, 2.2, 2.3, 1.5, 2.3, 2.0, 2.0, 1.8, 2.1, 1.8, 1.8, 1.8, 2.1, 1.6, 1.9, 2.0, 2.2, 1.5, 1.4, 2.3, 2.4, 1.8, 1.8, 2.1, 2.4, 2.3, 1.9, 2.3, 2.5, 2.3, 1.9, 2.0, 2.3, 1.8],
            //        ]
            //    });
            //}, 4000);
            //setTimeout(function () {
            //    pie_chart_lpo_wq1_field.unload({
            //        ids: 'data1'
            //    });
            //    pie_chart_lpo_wq1_field.unload({
            //        ids: 'data2'
            //    });
            //}, 8000);

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                pie_chart.resize();
            });
        }

        // Combined chart
        if (piep_oil_production_prognosis_element) {

            // Generate chart
            var piep_oil_production_prognosis = c3.generate({
                bindto: piep_oil_production_prognosis_element,
                size: { height: 400 },
                color: {
                    pattern: ['#FF9800', '#F44336', '#009688', '#4CAF50', '#03A9F4', '#8BC34A']
                },
                data: {
                    columns: [
                        ['Prognosa', 30, 20, 50, 40, 60, 50],
                        //['data2', 200, 130, 90, 240, 130, 220],
                        ['RKAP', 30, 20, 50, 40, 60, 50],
                        //['data4', 200, 130, 90, 240, 130, 220],
                        //['data5', 130, 120, 150, 140, 160, 150],
                        //['data6', 90, 70, 20, 50, 60, 120],
                    ],
                    type: 'bar',
                    types: {
                        RKAP: 'spline',
                        data4: 'line',
                        data6: 'area',
                    },
                    groups: [
                        ['Prognosa', 'data2']
                    ]
                }
            });

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                piep_oil_production_prognosis.resize();
            });
        }

        // Combined chart
        if (piep_gas_production_prognosis_element) {

            // Generate chart
            var piep_gas_production_prognosis = c3.generate({
                bindto: piep_gas_production_prognosis_element,
                size: { height: 400 },
                color: {
                    pattern: ['#FF9800', '#F44336', '#009688', '#4CAF50', '#03A9F4', '#8BC34A']
                },
                data: {
                    columns: [
                        ['Prognosa', 200, 130, 90, 240, 130, 220],
                        //['data2', 200, 130, 90, 240, 130, 220],
                        ['RKAP', 200, 130, 90, 240, 130, 220],
                        //['data4', 200, 130, 90, 240, 130, 220],
                        //['data5', 130, 120, 150, 140, 160, 150],
                        //['data6', 90, 70, 20, 50, 60, 120],
                    ],
                    type: 'bar',
                    types: {
                        RKAP: 'spline',
                        data4: 'line',
                        data6: 'area',
                    },
                    groups: [
                        ['Prognosa', 'data2']
                    ]
                }
            });

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                piep_gas_production_prognosis.resize();
            });
        }

        // Combined chart
        if (piep_oil_equivalent_production_prognosis_element) {

            // Generate chart
            var piep_oil_equivalent_production_prognosis = c3.generate({
                bindto: piep_oil_equivalent_production_prognosis_element,
                size: { height: 400 },
                color: {
                    pattern: ['#FF9800', '#F44336', '#009688', '#4CAF50', '#03A9F4', '#8BC34A']
                },
                data: {
                    columns: [
                        ['Prognosa', 130, 120, 150, 140, 160, 150],
                        //['data2', 200, 130, 90, 240, 130, 220],
                        ['RKAP', 130, 120, 150, 140, 160, 150],
                        //['data4', 200, 130, 90, 240, 130, 220],
                        //['data5', 130, 120, 150, 140, 160, 150],
                        //['data6', 90, 70, 20, 50, 60, 120],
                    ],
                    type: 'bar',
                    types: {
                        RKAP: 'spline',
                        data4: 'line',
                        data6: 'area',
                    },
                    groups: [
                        ['Prognosa', 'data2']
                    ]
                }
            });

            // Resize chart on sidebar width change
            $('.sidebar-control').on('click', function () {
                piep_oil_equivalent_production_prognosis.resize();
            });
        }
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _barsPiesExamples();
        }
    }
}();


var Modals = function () {


    //
    // Setup module components
    //

    // Load remote content
    var _componentModalRemote = function () {
        $('#modal_remote').on('show.bs.modal', function () {
            $(this).find('.modal-body').load('../../../../global_assets/demo_data/wizard/education.html', function () {
                _componentSelect2();
            });
        });
    };

    // Modal callbacks
    var _componentModalCallbacks = function () {

        // onShow callback
        $('#modal_onshow').on('show.bs.modal', function () {
            alert('onShow callback fired.')
        });

        // onShown callback
        $('#modal_onshown').on('shown.bs.modal', function () {
            alert('onShown callback fired.')
        });

        // onHide callback
        $('#modal_onhide').on('hide.bs.modal', function () {
            alert('onHide callback fired.')
        });

        // onHidden callback
        $('#modal_onhidden').on('hidden.bs.modal', function () {
            alert('onHidden callback fired.')
        });
    };

    // Bootbox extension
    var _componentModalBootbox = function () {
        if (typeof bootbox == 'undefined') {
            console.warn('Warning - bootbox.min.js is not loaded.');
            return;
        }

        // Alert dialog
        $('#alert').on('click', function () {
            bootbox.alert({
                title: 'Check this out!',
                message: 'Native alert dialog has been replaced with Bootbox alert box.'
            });
        });

        // Confirmation dialog
        $('#confirm').on('click', function () {
            bootbox.confirm({
                title: 'Confirm dialog',
                message: 'Native confirm dialog has been replaced with Bootbox confirm box.',
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-link'
                    }
                },
                callback: function (result) {
                    bootbox.alert({
                        title: 'Confirmation result',
                        message: 'Confirm result: ' + result
                    });
                }
            });
        });

        // Prompt dialog
        $('#prompt').on('click', function () {
            bootbox.prompt({
                title: 'Please enter your name',
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-link'
                    }
                },
                callback: function (result) {
                    if (result === null) {
                        bootbox.alert({
                            title: 'Prompt dismissed',
                            message: 'You have cancelled this damn thing'
                        });
                    } else {
                        bootbox.alert({
                            title: 'Hi <strong>' + result + '</strong>',
                            message: 'How are you doing today?'
                        });
                    }
                }
            });
        });

        // Prompt dialog with default value
        $('#prompt_value').on('click', function () {
            bootbox.prompt({
                title: 'What is your real name?',
                value: 'Eugene Kopyov',
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-primary'
                    },
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-link'
                    }
                },
                callback: function (result) {
                    if (result === null) {
                        bootbox.alert({
                            title: 'Prompt dismissed',
                            message: 'You have cancelled this damn thing'
                        });
                    } else {
                        bootbox.alert({
                            title: 'Hi <strong>' + result + '</strong>',
                            message: 'How are you doing today?'
                        });
                    }
                }
            });
        });

        // Custom bootbox dialog
        $('#bootbox_custom').on('click', function () {
            bootbox.dialog({
                message: 'I am a custom dialog',
                title: 'Custom title',
                buttons: {
                    success: {
                        label: 'Success!',
                        className: 'btn-success',
                        callback: function () {
                            bootbox.alert({
                                title: 'Success!',
                                message: 'This is a great success!'
                            });
                        }
                    },
                    danger: {
                        label: 'Danger!',
                        className: 'btn-danger',
                        callback: function () {
                            bootbox.alert({
                                title: 'Ohh noooo!',
                                message: 'Uh oh, look out!'
                            });
                        }
                    },
                    main: {
                        label: 'Click ME!',
                        className: 'btn-primary',
                        callback: function () {
                            bootbox.alert({
                                title: 'Hooray!',
                                message: 'Something awesome just happened!'
                            });
                        }
                    }
                }
            });
        });

        // Custom bootbox dialog with form
        $('#bootbox_form').on('click', function () {
            bootbox.dialog({
                title: 'This is a form in a modal.',
                message: '<div class="row">  ' +
                    '<div class="col-md-12">' +
                    '<form action="">' +
                    '<div class="form-group row">' +
                    '<label class="col-md-4 col-form-label">Name</label>' +
                    '<div class="col-md-8">' +
                    '<input id="name" name="name" type="text" placeholder="Your name" class="form-control">' +
                    '<span class="form-text text-muted">Here goes your name</span>' +
                    '</div>' +
                    '</div>' +
                    '<div class="form-group row">' +
                    '<label class="col-md-4 col-form-label">How awesome is this?</label>' +
                    '<div class="col-md-8">' +
                    '<div class="form-check">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" name="awesomeness" id="awesomeness-0" value="Really awesome" checked>' +
                    'Really awesomeness' +
                    '</label>' +
                    '</div>' +
                    '<div class="form-check">' +
                    '<label class="form-check-label">' +
                    '<input type="radio" class="form-check-input" name="awesomeness" id="awesomeness-1" value="Super awesome">' +
                    'Super awesome' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</form>' +
                    '</div>' +
                    '</div>',
                buttons: {
                    success: {
                        label: 'Save',
                        className: 'btn-success',
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $('input[name="awesomeness"]:checked').val()
                            bootbox.alert({
                                title: 'Hello ' + name + '!',
                                message: 'You have chosen <strong>' + answer + '</strong>.'
                            });
                        }
                    }
                }
            }
            );
        });
    };

    // Select2
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-control-select2').select2({
            minimumResultsForSearch: Infinity
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        initComponents: function () {
            _componentModalRemote();
            _componentModalCallbacks();
            _componentModalBootbox();
        }
    }
}();

var DateTimePickers = function () {


    //
    // Setup module components
    //

    // Daterange picker
    var _componentDaterange = function () {
        if (!$().daterangepicker) {
            console.warn('Warning - daterangepicker.js is not loaded.');
            return;
        }

        // Basic initialization
        $('.daterange-basic').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light'
        });

        // Display week numbers
        $('.daterange-weeknumbers').daterangepicker({
            showWeekNumbers: true,
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light'
        });

        // Button class options
        $('.daterange-buttons').daterangepicker({
            applyClass: 'btn-success',
            cancelClass: 'btn-danger'
        });

        // Display time picker
        $('.daterange-time').daterangepicker({
            timePicker: true,
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            locale: {
                format: 'MM/DD/YYYY h:mm a'
            }
        });

        // Show calendars on left
        $('.daterange-left').daterangepicker({
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light'
        });

        // Single picker
        $('.daterange-single').daterangepicker({
            singleDatePicker: true
        });

        // Display date dropdowns
        $('.daterange-datemenu').daterangepicker({
            showDropdowns: true,
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light'
        });

        // 10 minute increments
        $('.daterange-increments').daterangepicker({
            timePicker: true,
            opens: 'left',
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            timePickerIncrement: 10,
            locale: {
                format: 'MM/DD/YYYY h:mm a'
            }
        });

        // Localization
        $('.daterange-locale').daterangepicker({
            applyClass: 'bg-slate-600',
            cancelClass: 'btn-light',
            opens: 'left',
            ranges: {
                'Ð¡ÐµÐ³Ð¾Ð´Ð½Ñ': [moment(), moment()],
                'Ð’Ñ‡ÐµÑ€Ð°': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'ÐŸÐ¾ÑÐ»ÐµÐ´Ð½Ð¸Ðµ 7 Ð´Ð½ÐµÐ¹': [moment().subtract(6, 'days'), moment()],
                'ÐŸÐ¾ÑÐ»ÐµÐ´Ð½Ð¸Ðµ 30 Ð´Ð½ÐµÐ¹': [moment().subtract(29, 'days'), moment()],
                'Ð­Ñ‚Ð¾Ñ‚ Ð¼ÐµÑÑÑ†': [moment().startOf('month'), moment().endOf('month')],
                'ÐŸÑ€Ð¾ÑˆÐµÐ´ÑˆÐ¸Ð¹ Ð¼ÐµÑÑÑ†': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            locale: {
                applyLabel: 'Ð’Ð¿ÐµÑ€ÐµÐ´',
                cancelLabel: 'ÐžÑ‚Ð¼ÐµÐ½Ð°',
                startLabel: 'ÐÐ°Ñ‡Ð°Ð»ÑŒÐ½Ð°Ñ Ð´Ð°Ñ‚Ð°',
                endLabel: 'ÐšÐ¾Ð½ÐµÑ‡Ð½Ð°Ñ Ð´Ð°Ñ‚Ð°',
                customRangeLabel: 'Ð’Ñ‹Ð±Ñ€Ð°Ñ‚ÑŒ Ð´Ð°Ñ‚Ñƒ',
                daysOfWeek: ['Ð’Ñ', 'ÐŸÐ½', 'Ð’Ñ‚', 'Ð¡Ñ€', 'Ð§Ñ‚', 'ÐŸÑ‚', 'Ð¡Ð±'],
                monthNames: ['Ð¯Ð½Ð²Ð°Ñ€ÑŒ', 'Ð¤ÐµÐ²Ñ€Ð°Ð»ÑŒ', 'ÐœÐ°Ñ€Ñ‚', 'ÐÐ¿Ñ€ÐµÐ»ÑŒ', 'ÐœÐ°Ð¹', 'Ð˜ÑŽÐ½ÑŒ', 'Ð˜ÑŽÐ»ÑŒ', 'ÐÐ²Ð³ÑƒÑÑ‚', 'Ð¡ÐµÐ½Ñ‚ÑÐ±Ñ€ÑŒ', 'ÐžÐºÑ‚ÑÐ±Ñ€ÑŒ', 'ÐÐ¾ÑÐ±Ñ€ÑŒ', 'Ð”ÐµÐºÐ°Ð±Ñ€ÑŒ'],
                firstDay: 1
            }
        });


        //
        // Pre-defined ranges and callback
        //

        // Initialize with options
        $('.daterange-predefined').daterangepicker(
            {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2014',
                maxDate: '12/31/2019',
                dateLimit: { days: 60 },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                applyClass: 'btn-sm bg-slate',
                cancelClass: 'btn-sm btn-light'
            },
            function (start, end) {
                $('.daterange-predefined span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
                $.jGrowl('Date range has been changed', { header: 'Update', theme: 'bg-primary', position: 'center', life: 1500 });
            }
        );

        // Display date format
        $('.daterange-predefined span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));


        //
        // Inside button
        //

        // Initialize with options
        $('.daterange-ranges').daterangepicker(
            {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2019',
                dateLimit: { days: 60 },
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                applyClass: 'btn-sm bg-slate-600',
                cancelClass: 'btn-sm btn-light'
            },
            function (start, end) {
                $('.daterange-ranges span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
            }
        );

        // Display date format
        $('.daterange-ranges span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + moment().format('MMMM D, YYYY'));
    };

    // Pickadate picker
    var _componentPickadate = function () {
        if (!$().pickadate) {
            console.warn('Warning - picker.js and/or picker.date.js is not loaded.');
            return;
        }

        // Basic options
        $('.pickadate').pickadate();

        // Change day names
        $('.pickadate-strings').pickadate({
            weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            showMonthsShort: true
        });

        // Button options
        $('.pickadate-buttons').pickadate({
            today: '',
            close: '',
            clear: 'Clear selection'
        });

        // Accessibility labels
        $('.pickadate-accessibility').pickadate({
            labelMonthNext: 'Go to the next month',
            labelMonthPrev: 'Go to the previous month',
            labelMonthSelect: 'Pick a month from the dropdown',
            labelYearSelect: 'Pick a year from the dropdown',
            selectMonths: true,
            selectYears: true
        });

        // Localization
        $('.pickadate-translated').pickadate({
            monthsFull: ['Janvier', 'FÃ©vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'AoÃ»t', 'Septembre', 'Octobre', 'Novembre', 'DÃ©cembre'],
            weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
            today: 'aujourd\'hui',
            clear: 'effacer',
            formatSubmit: 'yyyy/mm/dd'
        });

        // Format options
        $('.pickadate-format').pickadate({

            // Escape any â€œruleâ€ characters with an exclamation mark (!).
            format: 'You selecte!d: dddd, dd mmm, yyyy',
            formatSubmit: 'yyyy/mm/dd',
            hiddenPrefix: 'prefix__',
            hiddenSuffix: '__suffix'
        });

        // Editable input
        var $input_date = $('.pickadate-editable').pickadate({
            editable: true,
            onClose: function () {
                $('.datepicker').focus();
            }
        });
        var picker_date = $input_date.pickadate('picker');
        $input_date.on('click', function (event) {
            if (picker_date.get('open')) {
                picker_date.close();
            } else {
                picker_date.open();
            }
            event.stopPropagation();
        });

        // Dropdown selectors
        $('.pickadate-selectors').pickadate({
            selectYears: true,
            selectMonths: true
        });

        // Year selector
        $('.pickadate-year').pickadate({
            selectYears: 4
        });

        // Set first weekday
        $('.pickadate-weekday').pickadate({
            firstDay: 1
        });

        // Date limits
        $('.pickadate-limits').pickadate({
            min: [2014, 3, 20],
            max: [2014, 7, 14]
        });

        // Disable certain dates
        $('.pickadate-disable').pickadate({
            disable: [
                [2015, 8, 3],
                [2015, 8, 12],
                [2015, 8, 20]
            ]
        });

        // Disable date range
        $('.pickadate-disable-range').pickadate({
            disable: [
                5,
                [2013, 10, 21, 'inverted'],
                { from: [2014, 3, 15], to: [2014, 3, 25] },
                [2014, 3, 20, 'inverted'],
                { from: [2014, 3, 17], to: [2014, 3, 18], inverted: true }
            ]
        });

        // Events
        $('.pickadate-events').pickadate({
            onStart: function () {
                console.log('Hello there :)')
            },
            onRender: function () {
                console.log('Whoa.. rendered anew')
            },
            onOpen: function () {
                console.log('Opened up')
            },
            onClose: function () {
                console.log('Closed now')
            },
            onStop: function () {
                console.log('See ya.')
            },
            onSet: function (context) {
                console.log('Just set stuff:', context)
            }
        });
    };

    // Pickatime picker
    var _componentPickatime = function () {
        if (!$().pickatime) {
            console.warn('Warning - picker.js and/or picker.time.js is not loaded.');
            return;
        }

        // Default functionality
        $('.pickatime').pickatime();

        // Clear button
        $('.pickatime-clear').pickatime({
            clear: ''
        });

        // Time formats
        $('.pickatime-format').pickatime({

            // Escape any â€œruleâ€ characters with an exclamation mark (!).
            format: 'T!ime selected: h:i a',
            formatLabel: '<b>h</b>:i <!i>a</!i>',
            formatSubmit: 'HH:i',
            hiddenPrefix: 'prefix__',
            hiddenSuffix: '__suffix'
        });

        // Send hidden value
        $('.pickatime-hidden').pickatime({
            formatSubmit: 'HH:i',
            hiddenName: true
        });

        // Editable input
        var $input_time = $('.pickatime-editable').pickatime({
            editable: true,
            onClose: function () {
                $('.datepicker').focus();
            }
        });
        var picker_time = $input_time.pickatime('picker');
        $input_time.on('click', function (event) {
            if (picker_time.get('open')) {
                picker_time.close();
            } else {
                picker_time.open();
            }
            event.stopPropagation();
        });

        // Time intervals
        $('.pickatime-intervals').pickatime({
            interval: 150
        });


        // Time limits
        $('.pickatime-limits').pickatime({
            min: [7, 30],
            max: [14, 0]
        });

        // Using integers as hours
        $('.pickatime-limits-integers').pickatime({
            disable: [
                3, 5, 7
            ]
        });

        // Disable times
        $('.pickatime-disabled').pickatime({
            disable: [
                [0, 30],
                [2, 0],
                [8, 30],
                [9, 0]
            ]
        });

        // Disabling ranges
        $('.pickatime-range').pickatime({
            disable: [
                1,
                [1, 30, 'inverted'],
                { from: [4, 30], to: [10, 30] },
                [6, 30, 'inverted'],
                { from: [8, 0], to: [9, 0], inverted: true }
            ]
        });

        // Disable all with exeption
        $('.pickatime-disableall').pickatime({
            disable: [
                true,
                3, 5, 7,
                [0, 30],
                [2, 0],
                [8, 30],
                [9, 0]
            ]
        });

        // Events
        $('.pickatime-events').pickatime({
            onStart: function () {
                console.log('Hello there :)')
            },
            onRender: function () {
                console.log('Whoa.. rendered anew')
            },
            onOpen: function () {
                console.log('Opened up')
            },
            onClose: function () {
                console.log('Closed now')
            },
            onStop: function () {
                console.log('See ya.')
            },
            onSet: function (context) {
                console.log('Just set stuff:', context)
            }
        });
    };

    // Anytime picker
    var _componentAnytime = function () {
        if (!$().AnyTime_picker) {
            console.warn('Warning - anytime.min.js is not loaded.');
            return;
        }

        // Basic usage
        $('#anytime-date').AnyTime_picker({
            format: '%W, %M %D in the Year %z %E',
            firstDOW: 1
        });

        // Time picker
        $('#anytime-time').AnyTime_picker({
            format: '%H:%i'
        });

        // Display hours only
        $('#anytime-time-hours').AnyTime_picker({
            format: '%l %p'
        });

        // Date and time
        $('#anytime-both').AnyTime_picker({
            format: '%M %D %H:%i',
        });

        // Custom display format
        $('#anytime-weekday').AnyTime_picker({
            format: '%W, %D of %M, %Z'
        });

        // Numeric date
        $('#anytime-month-numeric').AnyTime_picker({
            format: '%d/%m/%Z'
        });

        // Month and day
        $('#anytime-month-day').AnyTime_picker({
            format: '%D of %M'
        });

        // On demand picker
        $('#ButtonCreationDemoButton').on('click', function (e) {
            $('#ButtonCreationDemoInput').AnyTime_noPicker().AnyTime_picker().focus();
            e.preventDefault();
        });


        //
        // Date range
        //

        // Options
        var oneDay = 24 * 60 * 60 * 1000;
        var rangeDemoFormat = '%e-%b-%Y';
        var rangeDemoConv = new AnyTime.Converter({ format: rangeDemoFormat });

        // Set today's date
        $('#rangeDemoToday').on('click', function (e) {
            $('#rangeDemoStart').val(rangeDemoConv.format(new Date())).trigger('change');
        });

        // Clear dates
        $('#rangeDemoClear').on('click', function (e) {
            $('#rangeDemoStart').val('').trigger('change');
        });

        // Start date
        $('#rangeDemoStart').AnyTime_picker({
            format: rangeDemoFormat
        });

        // On value change
        $('#rangeDemoStart').on('change', function (e) {
            try {
                var fromDay = rangeDemoConv.parse($('#rangeDemoStart').val()).getTime();

                var dayLater = new Date(fromDay + oneDay);
                dayLater.setHours(0, 0, 0, 0);

                var ninetyDaysLater = new Date(fromDay + (90 * oneDay));
                ninetyDaysLater.setHours(23, 59, 59, 999);

                // End date
                $('#rangeDemoFinish')
                    .AnyTime_noPicker()
                    .removeAttr('disabled')
                    .val(rangeDemoConv.format(dayLater))
                    .AnyTime_picker({
                        earliest: dayLater,
                        format: rangeDemoFormat,
                        latest: ninetyDaysLater
                    });
            }

            catch (e) {

                // Disable End date field
                $('#rangeDemoFinish').val('').attr('disabled', 'disabled');
            }
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentDaterange();
            _componentPickadate();
            _componentPickatime();
            _componentAnytime();
        }
    }
}();
// Initialize module
// ------------------------------
document.addEventListener('DOMContentLoaded', function () {
    Select2Selects.init();
    EchartsPiesDonuts.init();
    InputsCheckboxesRadios.initComponents();
    С3BarsPies.init();
    EchartsColumnsWaterfalls.init();
    Modals.initComponents();
    DateTimePickers.init();
    BasicAlert.init();
    JqueryUiForms.init();
});
