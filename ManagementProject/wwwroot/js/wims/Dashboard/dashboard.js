﻿/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Pie and Donut charts
 *
 *  Demo JS code for echarts_pies_donuts.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var EchartsPiesDonuts = function () {


    //
    // Setup module components
    //

    // Pie and donut charts
    var _piesDonutsExamples = function () {
        if (typeof echarts == 'undefined') {
            console.warn('Warning - echarts.min.js is not loaded.');
            return;
        }

        //var basecolorred = "#DC143C";
        //var basecolorgreen = "#43A047";
        //var basecolororange = "#F4511E";
        //var basecoloryellow = "#f1c40f";
        //var basecolorblue = "#1E88E5";
        //var basecolorpurple = "#8e44ad";
        //var basecolorpink = "#D81B60";
        //var basecolorviolet = "#8E24AA";
        //var basecolorteal = "#00897B";

        var green = "#2ec7c9";
        var smoothpurple = "#b6a2de";
        var blue = "#5ab1ef";
        var smoothorange = "#ffb980";
        var smoothred = "#d87a80";
        var marked = "#8d98b3";
        var yellow = "#e5cf0d";
        var greenyellow = "#97b552";
        var brown = "#95706d";
        var pinkpurple = "#dc69aa";
        var darkgreen = "#07a2a4";
        var darkpurpler = "#9a7fd1";
        var darkblue = "#588dd5";
        var orange = "#f5994e";
        var brownred = "#c05050";
        var darkmarked = "#59678c";
        var yellowdark = "#c9ab00";
        var greentree = "#7eb00a";
        var brownsolid = "#6f5553";
        var darkpurple= "#c14089";

        // Define elements
        var dashboard_overview_pie_one_element = document.getElementById('dashboard-overview-pie-one');
        var dashboard_overview_pie_two_element = document.getElementById('dashboard-overview-pie-two');

        //
        // Charts configuration
        //

        // Basic pie chart
        if (dashboard_overview_pie_one_element) {

            // Initialize chart
            var dashboard_overview_pie_one = echarts.init(dashboard_overview_pie_one_element);


            //
            // Chart config
            //

            // Options
            dashboard_overview_pie_one.setOption({

                // Colors
                color: [
                    //basecolorgreen, basecoloryellow, basecolorred
                    green, yellow, smoothred, pinkpurple, blue, smoothpurple, smoothorange,  marked, 
                    greenyellow, brown,  darkgreen, darkpurpler, darkblue, orange,
                    brownred, darkmarked, yellowdark, greentree, brownsolid, darkpurple
                ],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Add title
                title: {
                    text: '',
                    subtext: '',
                    left: 'center',
                    textStyle: {
                        fontSize: 17,
                        fontWeight: 500
                    },
                    subtextStyle: {
                        fontSize: 12
                    }
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'vertical',
                    top: 'center',
                    left: 0,
                    data: ['Green', 'Yellow', 'Red'],
                    itemHeight: 8,
                    itemWidth: 8
                },

                // Add series
                series: [{
                    name: 'pie',
                    type: 'pie',
                    label: {
                        Show: true
                    },
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [
                        { value: 10, name: 'Green' },
                        { value: 0, name: 'Yellow' },
                        { value: 7, name: 'Red' },
                    ]
                }]
            });
        }

        if (dashboard_overview_pie_two_element) {

            // Initialize chart
            var dashboard_overview_pie_two = echarts.init(dashboard_overview_pie_two_element);


            //
            // Chart config
            //

            // Options
            dashboard_overview_pie_two.setOption({

                // Colors
                color: [
                    //basecolorgreen, basecoloryellow, basecolorred
                    green, yellow, blue, smoothred, pinkpurple,  smoothpurple, smoothorange, marked,
                    greenyellow, brown, darkgreen, darkpurpler, darkblue, orange,
                    brownred, darkmarked, yellowdark, greentree, brownsolid, darkpurple
                ],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Add title
                title: {
                    text: 'MLN',
                    subtext: 'Well Operation Status',
                    left: 'center',
                    textStyle: {
                        fontSize: 17,
                        fontWeight: 500
                    },
                    subtextStyle: {
                        fontSize: 12
                    }
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'vertical',
                    top: 'center',
                    right: 0,
                    data: ['Green', 'Yellow', 'Blue'],
                    itemHeight: 8,
                    itemWidth: 8
                },

                // Add series
                series: [{
                    name: 'pie',
                    type: 'pie',
                    label: {
                        Show: true
                    },
                    radius: '70%',
                    center: ['50%', '57.5%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [
                        { value: 5, name: 'Green' },
                        { value: 7, name: 'Yellow' },
                        { value: 5, name: 'Blue' },
                    ]
                }]
            });
        }

        //
        // Resize charts
        //

        // Resize function
        var triggerChartResize = function () {
            dashboard_overview_pie_one_element && dashboard_overview_pie_one.resize();
            dashboard_overview_pie_two_element && dashboard_overview_pie_two.resize();
        };

        // On sidebar width change
        $(document).on('click', '.sidebar-control', function () {
            setTimeout(function () {
                triggerChartResize();
            }, 0);
        });

        // On window resize
        var resizeCharts;
        window.onresize = function () {
            clearTimeout(resizeCharts);
            resizeCharts = setTimeout(function () {
                triggerChartResize();
            }, 200);
        };
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _piesDonutsExamples();
        }
    }
}();


/* ------------------------------------------------------------------------------
 *
 *  # Select2 selects
 *
 *  Specific JS code additions for form_select2.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var Select2Selects = function () {


    //
    // Setup module components
    //

    // Select2 examples
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Select with search
        $('.select-search').select2();

        //
        // Customize matched results
        //

        // Setup matcher
        function matchStart(term, text) {
            if (text.toUpperCase().indexOf(term.toUpperCase()) == 0) {
                return true;
            }

            return false;
        }

        // Initialize
        $.fn.select2.amd.require(['select2/compat/matcher'], function (oldMatcher) {
            $('.select-matched-customize').select2({
                minimumResultsForSearch: Infinity,
                placeholder: 'Select a State',
                matcher: oldMatcher(matchStart)
            });
        });

    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentSelect2();
        }
    }
}();

/* ------------------------------------------------------------------------------
 *
 *  # Checkboxes and radios
 *
 *  Demo JS code for form_checkboxes_radios.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var InputsCheckboxesRadios = function () {


    //
    // Setup components
    //

    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Default initialization
        $('.form-check-input-styled').uniform();


        //
        // Contextual colors
        //

        // Primary
        $('.form-check-input-styled-primary').uniform({
            wrapperClass: 'border-primary-600 text-primary-800'
        });

        // Danger
        $('.form-check-input-styled-danger').uniform({
            wrapperClass: 'border-danger-600 text-danger-800'
        });

        // Success
        $('.form-check-input-styled-success').uniform({
            wrapperClass: 'border-success-600 text-success-800'
        });

        // Warning
        $('.form-check-input-styled-warning').uniform({
            wrapperClass: 'border-warning-600 text-warning-800'
        });

        // Info
        $('.form-check-input-styled-info').uniform({
            wrapperClass: 'border-info-600 text-info-800'
        });

        // Custom color
        $('.form-check-input-styled-custom').uniform({
            wrapperClass: 'border-indigo-600 text-indigo-800'
        });
    };

    // Switchery
    var _componentSwitchery = function () {
        if (typeof Switchery == 'undefined') {
            console.warn('Warning - switchery.min.js is not loaded.');
            return;
        }

        // Initialize multiple switches
        var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
        elems.forEach(function (html) {
            var switchery = new Switchery(html);
        });

        // Colored switches
        var primary = document.querySelector('.form-check-input-switchery-primary');
        var switchery = new Switchery(primary, { color: '#2196F3' });

        var danger = document.querySelector('.form-check-input-switchery-danger');
        var switchery = new Switchery(danger, { color: '#EF5350' });

        var warning = document.querySelector('.form-check-input-switchery-warning');
        var switchery = new Switchery(warning, { color: '#FF7043' });

        var info = document.querySelector('.form-check-input-switchery-info');
        var switchery = new Switchery(info, { color: '#00BCD4' });
    };

    // Bootstrap switch
    var _componentBootstrapSwitch = function () {
        if (!$().bootstrapSwitch) {
            console.warn('Warning - switch.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-check-input-switch').bootstrapSwitch();
    };


    //
    // Return objects assigned to module
    //

    return {
        initComponents: function () {
            _componentUniform();
            _componentSwitchery();
            _componentBootstrapSwitch();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    EchartsPiesDonuts.init();
    Select2Selects.init();
    InputsCheckboxesRadios.initComponents();
});
