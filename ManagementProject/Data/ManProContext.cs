﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Threading;
using System.Threading.Tasks;
using ManagementProject.Models;

namespace ManagementProject.Models
{
    public class ManProContext : DbContext
    {
        public ManProContext (DbContextOptions<ManProContext> options)
            : base(options)
        {
        }

        public DbSet<ManagementProject.Models.ProjectPlan> ProjectPlan { get; set; }

        public DbSet<ManagementProject.Models.Source> Source { get; set; }

        public DbSet<ManagementProject.Models.ProjectType> ProjectType { get; set; }

        public DbSet<ManagementProject.Models.ProjectPlanStep> ProjectPlanStep { get; set; }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            foreach (EntityEntry<Global> entry in ChangeTracker.Entries<Global>())
            {
                // If the entity was added.
                if (entry.State == EntityState.Added)
                {
                    //entry.Property("ROW_CREATED_BY").CurrentValue = "xxx";
                    entry.Property("ROW_CREATED_DATE").CurrentValue = DateTime.Now;
                    entry.Property("PPDM_GUID").CurrentValue = Guid.NewGuid().ToString();
                }
                else if (entry.State == EntityState.Modified) // If the entity was updated
                {
                    //entry.Property("ROW_CHANGED_BY").CurrentValue = "xxx";
                    entry.Property("ROW_CHANGED_DATE").CurrentValue = DateTime.Now;
                    entry.Property("ROW_CREATED_DATE").IsModified = false;
                    entry.Property("ROW_CREATED_BY").IsModified = false;
                    entry.Property("PPDM_GUID").IsModified = false;
                }
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public DbSet<ManagementProject.Models.ProjectStatusType> ProjectStatusType { get; set; }

        public DbSet<ManagementProject.Models.ProjectStatus> ProjectStatus { get; set; }

        public DbSet<ManagementProject.Models.ProjectCondition> ProjectCondition { get; set; }

        public DbSet<ManagementProject.Models.ProjectStep> ProjectStep { get; set; }

        public DbSet<ManagementProject.Models.Project> Project { get; set; }

        //public DbSet<ManagementProject.Models.RProjectStatus> ProjectStatus { get; set; }
    }
}
