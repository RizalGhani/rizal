﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementProject.Models
{
    [Table("PROJECT_STATUS")]
    public class ProjectStatus : Global
    {
        [Key]
        [Required]
        [StringLength(40, MinimumLength = 5)]
        [DisplayName("Status ID")]
        public string STATUS_ID { get; set; }

        [ForeignKey("PROJECT_ID")]
        //[Required]
        //[StringLength(40, MinimumLength = 5)]
        [DisplayName("Project ID")]
        public string PROJECT_ID { get; set; }

        //[Required]
        //[StringLength(40, MinimumLength = 5)]
        //[DisplayName("Source")]
        //public string SOURCE { get; set; }

        //[Required]
        //[StringLength(40, MinimumLength = 5)]
        [DisplayName("Status")]
        public string STATUS { get; set; }

        //[Required]
        //[StringLength(40, MinimumLength = 5)]
        [DisplayName("Step ID")]
        public string STEP_ID { get; set; }
        
        //public Source Source_ { get; set; }
        //public ProjectStep ProjectStep { get; set; }

    }
}
