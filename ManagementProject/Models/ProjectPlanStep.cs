﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManagementProject.Models
{
    [Table("PROJECT_PLAN_STEP")]
    public class ProjectPlanStep : Global
    {
        [Key]
        [Required]
        [StringLength(40, MinimumLength = 5)]
        [DisplayName("Plan Step ID")]
        public string PLAN_STEP_ID { get; set; }
        [ForeignKey("PROJECT_PLAN_ID")]
        [DisplayName("Plan ID")]
        public string PROJECT_PLAN_ID { get; set; }
        [Required]
        [StringLength(240, MinimumLength = 10)]
        [DisplayName("Description")]
        public string DESCRIPTION { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 10)]
        [DisplayName("Step Name")]
        public string STEP_NAME { get; set; }
        [Required]
        [StringLength(240, MinimumLength = 10)]
        [DisplayName("Step Rule")]
        public string STEP_RULE { get; set; }
        [Required]
        [DefaultValue(true)]
        [DisplayName("Step Seq Number")]
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = false)]
        public decimal STEP_SEQ_NO { get; set; }
        public string STEP_TYPE { get; set; }
    }
}
