﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManagementProject.Models
{
    [Table("PROJECT_PLAN")]
    public class ProjectPlan : Global
    {
        [Key]
        [Required]
        [StringLength(40, MinimumLength = 5)]
        [DisplayName("Plan ID")]
        public string PROJECT_PLAN_ID { get; set; }
        [Required]
        [StringLength(240, MinimumLength = 10)]
        [DisplayName("Description")]
        public string DESCRIPTION { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 10)]
        [DisplayName("Plan Name")]
        public string PLAN_NAME { get; set; }
        [ForeignKey("PROJECT_TYPE")]
        [DisplayName("Project Type")]
        public string PROJECT_TYPE { get; set; }
    }
}