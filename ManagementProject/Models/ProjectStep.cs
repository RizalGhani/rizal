﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace ManagementProject.Models
{
    [Table("PROJECT_STEP")]
    public class ProjectStep : Global
    {
        [Key]
        [Required]
        [DisplayName("Step ID")]
        [StringLength(40, MinimumLength = 5)]
        public string STEP_ID { get; set; }

        [Required]
        [DisplayName("Project ID")]
        [StringLength(40, MinimumLength = 5)]
        public string PROJECT_ID { get; set; }

        [Required]
        [DisplayName("Plan Step ID")]
        [StringLength(40, MinimumLength = 5)]
        public string PLAN_STEP_ID { get; set; }

        [Required]
        [DisplayName("Project Plan ID")]
        [StringLength(40, MinimumLength = 5)]
        public string PROJECT_PLAN_ID { get; set; }

        [DisplayName("Step Name")]
        [StringLength(40, MinimumLength = 5)]
        public string STEP_NAME { get; set; }

        //[Required]
        //[DefaultValue(true)]
        //[DisplayName("Step Seq Number")]
        //[DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = false)]
        //public decimal STEP_SEQ_NO { get; set; }

        public ProjectStatus ProjectStatus { get; set; }
    }
}
