﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementProject.Models
{
    [Table("PROJECT")]
    public class Project : Global
    {
        [Key]
        [Required]
        [DisplayName("Project ID")]
        [StringLength(40, MinimumLength = 5)]
        public string PROJECT_ID { get; set; }
        [DisplayName("Project Type")]
        [StringLength(40, MinimumLength = 5)]
        public string PROJECT_TYPE { get; set; }
        public DateTime START_DATE { get; set; }
        public DateTime COMPLETE_DATE { get; set; }
    }
}
