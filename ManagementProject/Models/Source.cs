﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManagementProject.Models
{
    [Table("R_SOURCE")]
    public class Source 
    {
        [Key]
        [DisplayName("Source")]
        [Required]
        [StringLength(40, MinimumLength = 5)]
        [Column("SOURCE")]
        public string SOURCEID { get; set; } 
        [DisplayName("Abbreviation")]
        [Required]
        [StringLength(12, MinimumLength=5)]
        public string ABBREVIATION { get; set; }
        [Required]
        [DisplayName("Long Name")]
        [StringLength(30, MinimumLength = 5)]
        public string LONG_NAME { get; set; }
        [Required]
        [DisplayName("Short Name")]
        [StringLength(40, MinimumLength = 5)]
        public string SHORT_NAME { get; set; }
        public string ROW_SOURCE { get; set; }

        public string ACTIVE_IND { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string PPDM_GUID { get; set; } = Guid.NewGuid().ToString();
        public string REMARK { get; set; }
        public string ROW_CHANGED_BY { get; set; }
        public DateTime? ROW_CHANGED_DATE { get; set; }
        public string ROW_CREATED_BY { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMMM, yyyy}")]
        public DateTime? ROW_CREATED_DATE { get; set; }
        public DateTime? ROW_EFFECTIVE_DATE { get; set; }
        public DateTime? ROW_EXPIRY_DATE { get; set; }
        public string ROW_QUALITY { get; set; }
    }
}
