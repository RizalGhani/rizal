﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManagementProject.Models
{
    public class Global
    {
        public string ACTIVE_IND { get; set; }
        public string EFFECTIVE_DATE { get; set; }
        public string EXPIRY_DATE { get; set; }
        public string PPDM_GUID { get; set; }
        public string REMARK { get; set; }
        [Column("SOURCE")]
        [ForeignKey("SOURCE")]
        [DisplayName("Source")]
        public string SOURCEID { get; set; }
        public string ROW_CHANGED_BY { get; set; }
        public DateTime? ROW_CHANGED_DATE { get; set; }
        public string ROW_CREATED_BY { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd MMMM, yyyy}")]
        public DateTime? ROW_CREATED_DATE { get; set; }
        public DateTime? ROW_EFFECTIVE_DATE { get; set; }
        public DateTime? ROW_EXPIRY_DATE { get; set; }
        public string ROW_QUALITY { get; set; }
    }
}
