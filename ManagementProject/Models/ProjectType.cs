﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ManagementProject.Models
{
    [Table("R_PROJECT_TYPE")]
    public class ProjectType : Global
    {
        [Key]
        [Required]
        [StringLength(40, MinimumLength = 5)]
        [DisplayName("Project Type")]
        public string PROJECT_TYPE { get; set; }
        [Required]
        [DisplayName("Abbreviation")]
        [StringLength(12, MinimumLength = 5)]
        public string ABBREVIATION { get; set; }
        [Required]
        [DisplayName("Long Name")]
        [StringLength(30, MinimumLength = 5)]
        public string LONG_NAME { get; set; }
        [Required]
        [DisplayName("Short Name")]
        [StringLength(40, MinimumLength = 5)]
        public string SHORT_NAME { get; set; }
    }
}